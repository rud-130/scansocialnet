"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package twitter.functions
#
# Functions funciones  que seran usadas en las vistas
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

import time
import tweepy
from tweepy import OAuthHandler
import json

from rest.models import (
    UserSocial, Contact, UserContact
    )
from config.models import CredentialTwitter

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def authTwitter():
    """
        Funcion para definir el ususario de Twitter
    """
    credential = CredentialTwitter.objects(is_active=True).first()
    auth = OAuthHandler(credential.consumer_key, credential.consumer_secret)
    auth.set_access_token(credential.access_token, credential.access_token_secret)

    try:
        redirect_url = auth.get_authorization_url()
    except tweepy.TweepError:
        raise Exception ('Credenciales de Twitter invalidas')

    api = tweepy.API(auth, wait_on_rate_limit=True,
                     wait_on_rate_limit_notify=True)
    return api


def userTimeLine(username, count=1):
    """
        Funcion para realizar el time line del usuario objetivo
    """
    api = authTwitter()
    tweets = api.user_timeline(screen_name=username,
                               count=count, include_rts=True,
                               exclude_replies=False,
                               tweet_mode="extended")
    return tweets


def statusTweet(id_of_tweet):
    """
    Funcion para obtener un tweet especifico
    """
    api = authTwitter()
    tweet = api.get_status(id_of_tweet)
    return tweet


def usersFollowers(username):
    """
        Funcion para listar los usurios seguidores(followers) del usuario objetivo
    """
    api = authTwitter()
    followers = tweepy.Cursor(api.followers, screen_name=username).items()
    return followers


def usersFollowings(username):
    """
        Funcion para listar los usuarios que el usuario objetivo esta seguiendo(followings) 
    """
    api = authTwitter()
    followings = tweepy.Cursor(api.friends, screen_name=username).items()
    return followings


def getUser(username=None, identi=None):
    """
        Funcion que obtine informacion basica del usuario objetivo
    """
    api = authTwitter()
    if username is not None:
        get_user = api.get_user(screen_name=username)
    elif identi is not None:
        get_user = api.get_user(id=identi)
    return get_user


def searchFollowers(followers, user_target, user,
                                    contact = [],
                                    contacts_follow = []):
    """
        Funcion que permite buscar los Followers de un usaurio objetivo
    """
    contact += {'success': True},
    i = 1
    longitud = int(user_target.followers_count)
    printProgressBar(i, longitud, prefix = 'Progress:', suffix = 'Complete', length = 50)
    while True:
        try:
            follow = next(followers)
        except tweepy.TweepError:
            follow = next(followers)
        except StopIteration:
            break
        url = 'https://twitter.com/'
        url = url + follow.screen_name
        contact +=  {'relationship_user_id': user_target.id,
                             'tipo': 'Followers',
                             'name': follow.name.encode(),
                             'url': url},
        #Creando Contactos del usuario objetivo

        new_contact = Contact.objects(
                                                tipo='Followers',
                                                name=follow.name).upsert_one(
                                                tipo='Followers',
                                                name=follow.name,
                                                url=url,
                                                )


        user_contact = UserContact.objects(user=user, contact=new_contact).upsert_one(user=user, contact=new_contact)

        target_user = follow.screen_name
        contacts_follow += target_user,
        i +=1
        printProgressBar(i, longitud, prefix = 'Progress:', suffix = 'Complete', length = 50)
        if i % 180 == 0:
            time.sleep(61)
    return contact, contacts_follow


def searchFollowings(followings, user_target, user,
                                    contact = [],
                                    contacts_following = []):
    """
        Funcion que permite buscar los Followers de un usaurio objetivo
    """
    contact += {'success': True},
    i = 0
    longitud = int(user_target.friends_count)
    printProgressBar(i, longitud, prefix = 'Progress:', suffix = 'Complete', length = 50)
    while True:
        try:
            following = next(followings)
        except tweepy.TweepError:
            following = next(followings)
        except StopIteration:
            break
        url = 'https://twitter.com/'
        url = url + following.screen_name
        contact +=  {'relationship_user_id': user_target.id,
                             'tipo': 'Followings',
                             'name': following.name.encode(),
                             'url': url},

        #Creando Contactos del usuario objetivo

        new_contact = Contact.objects(
                                                tipo='Followings',
                                                name=following.name).upsert_one(
                                                tipo='Followings',
                                                name=following.name,
                                                url=url,
                                                )

        user_contact = UserContact.objects(user=user, contact=new_contact).upsert_one(user=user, contact=new_contact)

        target_user = following.screen_name
        contacts_following += target_user,
        i +=1
        printProgressBar(i, longitud, prefix = 'Progress:', suffix = 'Complete', length = 50)
        if i % 180 == 0:
            time.sleep(61)

    return contact, contacts_following


def favoriteTweet(tweet):
    api = authTwitter()
    favorites = api.favorites(tweet)
    return favorites
