"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package twitter.views
#
# Views de las vista genericas de la aplicacion rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

import re
import time
import tweepy
from rest_framework import viewsets, views, permissions
from rest_framework.response import Response
from rest.models import (
    UserSocial, Post, Comment,
    Contact, UserContact
    )
from rest.serializers import (
    PostRelatedSerializer, ContactSerializer
    )
from .functions import (
    userTimeLine, statusTweet,
    usersFollowers, usersFollowings, getUser,
    searchFollowers, searchFollowings, favoriteTweet
    )
from users.authentication import TokenAuthentication


class TwitterUserView(viewsets.ViewSet):
    """
        Class costumizada para obtener los datos de un usuario objetivo de la red Social Twitter

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 06/03/2018
        # @version 1.0.0
        > Parameters:

          * Create: POST twitter/
          * Consult Especific for username: GET twitter/?account=@usuario_objetivo ó twitter/?account=usuario_objetivo
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos guardados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.GET.get('account')
        if account is not None:
            if '@' in account:
                account = account.replace('@', '')
            try:
                queryset = UserSocial.objects.get(username=account)
                post_query = Post.objects(user=queryset)
            except Exception as e:
                raise e
            else:
                pass
            finally:
                pass
            serializer = PostRelatedSerializer(post_query, many=True)

            return Response(serializer.data)
        else:
            return Response('Debes enviar el nombre de la cuenta, con el parametro en la url asi: twitter/?account=@nombre_usuario ó twitter/?account=nombre_usuario')

    def create(self, request, format=None):
        """
            Método de la petición por post para listar los datos rastreados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.data.get('account')

        if account is not None:
            if '@' in account:
                account = account.replace('@', '')
            try:
                try:
                    get_user = getUser(account)
                except Exception as e:
                    msj = str(e)
                    tweet = {'success':False, 'mensaje': msj}
                    return Response(tweet)
                followers_count = int(get_user.followers_count)
                statuses_count = get_user.statuses_count
                friends_count = get_user.friends_count
                description = get_user.description
                user = get_user.name
                user_id = get_user.id
                location = get_user.location
                screen_name = get_user.screen_name
                profile_image_url = get_user.profile_image_url_https.replace("_normal.jpg", "_400x400.jpg")
                UserSocial.objects(id_user=str(user_id)).upsert_one(tipo_social='Twitter',
                                                        username=screen_name, img=profile_image_url)
                tweet = {
                             'success':True,
                              'data_user':
                                    {
                                         'user_id': user_id,
                                         'user': user,
                                         'statuses_count': statuses_count,
                                         'friends_count': friends_count,
                                         'followers_count': followers_count,
                                         'location': location,
                                         'description': description,
                                         'profile_image_url': profile_image_url,
                                         'username':screen_name,
                                     },
                                }
            except:
                msj = 'El usuario <b>%s</b>, que esta intentando rastrear no se encuentra registrado en Twitter.' % (account)
                tweet = {'success':False, 'mensaje': msj}
            return Response(tweet)
        else:
            return Response('Debes enviar el nombre de la cuenta, con el parametro account=@nombre_usuario ó account=nombre_usuario')


class TwitterPostView(viewsets.ViewSet):
    """
        Class costumizada para obtener los posts de  un usuario objetivo de la red Social Twitter

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 06/03/2018
        # @version 1.0.0
        > Parameters:

          * Create: POST twitter/posts
          * Consult Especific for username: GET twitter/posts?account=@usuario_objetivo ó twitter/?account=usuario_objetivo
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def create(self, request, format=None):
        account = request.data.get('account')
        count = request.data.get('count')
        if account is not None and count is not None:
            if '@' in account:
                account = account.replace('@', '')
            tweets = userTimeLine(account, count)
            get_user = getUser(account)
            user_id = get_user.id
            user = get_user.name
            screen_name = get_user.screen_name
            profile_image_url = get_user.profile_image_url_https.replace("_normal.jpg", "_400x400.jpg")
            new_user = UserSocial.objects(id_user=str(user_id)).upsert_one(tipo_social='Twitter',
                                                    username=screen_name, img=profile_image_url)
            tweet = []
            for status in tweets:
                    media = status.entities.get('media', [])
                    if(len(media) > 0):
                        media_url = media[0]['media_url']
                    else:
                        media_url = None
                    text = status._json['full_text']
                    regex_text = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', text)
                    create = status.created_at
                    retweet_count = status.retweet_count
                    favorite_count = status.favorite_count
                    tweet_id = status.id
                    if 'RT @' in text:
                        new_post = Post.objects(id_post=str(tweet_id), user=new_user).upsert_one(
                                                    historia='Retweet',
                                                    mensaje=status.full_text,
                                                    fecha_creacion=create,
                                                    img=media_url,
                                                    shares=retweet_count,
                                                    reaction=favorite_count
                                                    )
                        tweet += {'id_tweet': tweet_id,
                                        'retweet': regex_text.encode(),
                                        'create': create,
                                        'retweet_count': retweet_count,
                                        'favorite_count': favorite_count,
                                        'media_url': media_url},

                    elif status.in_reply_to_status_id is not None:
                        new_user_from_comment = UserSocial.objects(id_user=status.in_reply_to_user_id_str).upsert_one(
                                                        tipo_social='Twitter',
                                                        username=status.in_reply_to_screen_name)
                        status_tweet = statusTweet(status.in_reply_to_status_id)
                        new_post = Post.objects(id_post=status.in_reply_to_status_id_str, user=new_user_from_comment).upsert_one(
                                                    historia='Tweet',
                                                    mensaje=status.full_text,
                                                    fecha_creacion=status_tweet.created_at,
                                                    img=media_url,
                                                    shares=status_tweet.retweet_count,
                                                    reaction=status_tweet.favorite_count
                        )
                        new_comment_user = Comment.objects(id_comment=str(tweet_id), post=new_post).upsert_one(
                                                                                post=new_post,
                                                                                mensaje=status.full_text,
                                                                                author=user,
                                                                                fecha_creacion=create
                                                                                )
                        tweet += {'id_tweet': tweet_id,
                                  'reply': regex_text.encode(),
                                  'create': create,
                                  'retweet_count': retweet_count,
                                  'favorite_count': favorite_count,
                                  'reply_user': status.in_reply_to_user_id,
                                  'reply_screen_name':  status.in_reply_to_screen_name,
                                  'media_url': media_url},
                    else:
                        new_post = Post.objects(id_post=str(tweet_id), user=new_user).upsert_one(
                                                    historia='Tweet',
                                                    mensaje=status.full_text,
                                                    fecha_creacion=create,
                                                    img=media_url,
                                                    shares=retweet_count,
                                                    reaction=favorite_count
                                                    )
                        tweet += {'id_tweet': tweet_id,
                                        'tweet': regex_text.encode(),
                                        'create': create,
                                        'retweet_count': retweet_count,
                                        'favorite_count': favorite_count,
                                        'media_url': media_url},
            return Response(tweet)
        else:
            return Response('Debes enviar el nombre de la cuenta, con el parametro account=@nombre_usuario ó account=nombre_usuario')


class TwitterFollower(viewsets.ViewSet):
    """
        Class costumizada para obtener los Follower de un usuario objetivo de la red Social Twitter

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 06/03/2018
        # @version 1.0.0
        > Parameters:

          * Create: POST twitter/contacts/follower

    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def create(self, request, format=None):
        """
            Método de la petición por post
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.data.get('account')
        if account is not None:
            if '@' in account:
                account = account.replace('@', '')

            followers = usersFollowers(account)
            get_user = getUser(account)
            user = UserSocial.objects.get(id_user=str(get_user.id))
            print(user)
            follow = searchFollowers(followers, get_user, user)
            listFin = follow[1]
            contact = follow[0]
            """
            for contacto in listFin:
                try:
                    account = getUser(contacto)
                    cuenta = "Escaneando cuenta: %s" %(account.screen_name)
                    print(cuenta)
                    # Creando Nuevo Usuario
                    new_user = UserSocial.objects(id_user=str(account.id)).upsert_one(tipo_social='Twitter',
                                                        username=account.screen_name, img =account.profile_image_url_https.replace("_normal.jpg", "_400x400.jpg"))
                    followers = usersFollowers(account.screen_name)
                    follow = searchFollowers(followers, account, new_user)
                except tweepy.TweepError:
                    time.sleep(60 * 15)
                    print("Error Tiempo")
                    continue
            """
            return Response(contact)
        else:
            return Response('Debes enviar el nombre de la cuenta, con el parametro account=@nombre_usuario ó account=nombre_usuario')


class TwitterFollowing(viewsets.ViewSet):
    """
        Class costumizada para obtener los Following de un usuario objetivo de la red Social Twitter

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 06/03/2018
        # @version 1.0.0
        > Parameters:

          * Create: POST twitter/contacts/following
          
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def create(self, request, format=None):
        """
            Método de la petición por post
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.data.get('account')
        if account is not None:
            if '@' in account:
                account = account.replace('@', '')

            followings = usersFollowings(account)
            get_user = getUser(account)
            user = UserSocial.objects.get(id_user=str(get_user.id))
            print(user)

            following =searchFollowings(followings, get_user, user)
            listFin = following[1]
            contact = following[0]
            """
            for contacto in listFin:
                try:
                    time.sleep(15)
                    
                    account = getUser(contacto)
                    cuenta = "Escaneando cuenta: %s" %(account.screen_name)
                    print(cuenta)
                    # Creando Nuevo Usuario
                    new_user = UserSocial.objects(id_user=str(account.id)).upsert_one(tipo_social='Twitter',
                                                        username=account.screen_name, img=account.profile_image_url_https.replace("_normal.jpg", "_400x400.jpg"))
                    followings = usersFollowings(account.screen_name)
                    following = searchFollowings(followings, account, new_user)

                except tweepy.TweepError:
                    time.sleep(60 * 15)
                    print("Error Tiempo")
                    continue
            """
            return Response(contact)
        else:
            return Response('Debes enviar el nombre de la cuenta, con el parametro account=@nombre_usuario ó account=nombre_usuario')


class TwitterFollowerFollowingView(viewsets.ViewSet):
    """
        Class costumizada para obtener los contactos de un usuario objetivo de la red Social Twitter

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 06/03/2018
        # @version 1.0.0
        > Parameters:

          * Create: POST twitter/contacts/
          * Consult Especific for username: GET twitter/contacts/?account=@usuario_objetivo ó twitter/contacts/?account=usuario_objetivo
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos guardados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.GET.get('account')
        print(account)
        if account is not None:
            if '@' in account:
                account = account.replace('@', '')
            try:
                queryset = UserSocial.objects.get(username=account)
                post_query = Contact.objects(user=queryset)
            except Exception as e:
                raise e
            else:
                pass
            finally:
                pass
            serializer = ContactSerializer(post_query, many=True)

            return Response(serializer.data)
        else:
            return Response('Debes enviar el nombre de la cuenta, con el parametro en la url asi: twitter/?account=@nombre_usuario ó twitter/?account=nombre_usuario')

    def create(self, request, format=None):
        """
            Método de la petición por post
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.POST.get('account')
        print(account)
        url = 'https://twitter.com/'
        if account is not None:
            if '@' in account:
                account = account.replace('@', '')

            followers = usersFollowers(account)
            followings = usersFollowings(account)
            get_user = getUser(account)
            user = UserSocial.objects.get(id_user=str(get_user.id))

            follow = searchFollowers(followers, get_user, user)
            following =searchFollowings(followings, get_user, user)

            listIgual = set(following[1]) & set(follow[1])
            listDiff = set(following[1]) - set(follow[1])
            listFin = list(listIgual) + list(listDiff)
            contact = follow[0] + following[0]
            
            for contacto in listFin:
                try:
                    time.sleep(5)
                    account = getUser(contacto)
                    # Creando Nuevo Usuario
                    new_user = UserSocial.objects(id_user=str(account.id)).upsert_one(tipo_social='Twitter',
                                                        username=account.screen_name)
                    followers = usersFollowers(account.screen_name)
                    followings = usersFollowings(account.screen_name)
                    follow = searchFollowers(followers, account, new_user)
                    following = searchFollowings(followings, account, new_user)
                    time.sleep(5)
                    print(account.screen_name)
                except tweepy.TweepError:
                    time.sleep(60 * 15)
                    print("Error Tiempo")
                    continue
            return Response(contact)
