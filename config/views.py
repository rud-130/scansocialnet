"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package rest.views
#
# Views de las vista genericas de la aplicacion rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from rest_framework import viewsets, permissions
from rest_framework.response import Response
from django.core.validators import validate_email

from .models import *
from .serializers import *
from users.authentication import TokenAuthentication

class CredentialTwitterViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving CredentialTwitter.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

    * Create: POST credential/twitter/ => **consumer_key, consumer_secret, access_token, access_token_secret**.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    serializer_class = CredentialTwitterSerializer

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos guardados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        queryset = CredentialTwitter.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, format=None):
        """
            Método para el registro de credenciales de twitter
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        serialized = self.serializer_class(data=request.data)
        if serialized.is_valid():
            count = CredentialTwitter.objects.all().count()

            if count == 0:
                credential_twitter = None
            elif count > 1:
                CredentialTwitter.objects.all().delete()
                credential_twitter = None
            else:
                credential_twitter = CredentialTwitter.objects.all().latest('id')
                credential_twitter = credential_twitter.id

            if credential_twitter is not None:
                CredentialTwitter.objects(id=credential_twitter).upsert_one(consumer_key=serialized.data['consumer_key'],
                                    consumer_secret=serialized.data['consumer_secret'],
                                    access_token=serialized.data['access_token'],
                                    access_token_secret=serialized.data['access_token_secret']
                                    )
                return Response(serialized.data)
            else:
                credential_new = CredentialTwitter(
                                    consumer_key=serialized.data['consumer_key'],
                                    consumer_secret=serialized.data['consumer_secret'],
                                    access_token=serialized.data['access_token'],
                                    access_token_secret=serialized.data['access_token_secret'])
                credential_new.save()
                return Response(serialized.data)

            return Response(serialized.data)
        else:
            return Response(serialized._errors)


class CredentialFacebookViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving CredentialFacebook.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

    * Create: POST credential/facebook/ => **email_phone, password**.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    serializer_class = CredentialFacebookSerializer

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos guardados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        queryset = CredentialFacebook.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, format=None):
        """
            Método para el registro de credenciales de facebook
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        serialized = self.serializer_class(data=request.data)
        if serialized.is_valid():
            user = serialized.data['email_phone']
            validate_mail = False
            validate_phone = False
            error = {}
            error['email_phone'] = []
            try:
                validate_email(serialized.data['email_phone'])
                validate_mail = True
            except Exception as e:
                print (e)
                validate_mail = False
                error['email_phone'] += e,
            if user.isdigit():
                validate_phone = True
            else:
                validate_phone = False
                error['email_phone'] += ['Debe ingresar un numero de telefono valido'],

            count = CredentialFacebook.objects.all().count()

            if count == 0:
                credential_face = None
            elif count > 1:
                CredentialFacebook.objects.all().delete()
                credential_face = None
            else:
                credential_face = CredentialFacebook.objects.all().latest('id')
                credential_face = credential_face.id

            if validate_mail or validate_phone:
                if credential_face is not None:
                    CredentialFacebook.objects(id=credential_face).upsert_one(email_phone=serialized.data['email_phone'],
                                        password=serialized.data['password'])
                    return Response(serialized.data)
                else:
                    credential_new = CredentialFacebook(email_phone=serialized.data['email_phone'],
                                        password=serialized.data['password'])
                    credential_new.save()
                    return Response(serialized.data)
            else:
                return Response({'error': error})
        else:
            return Response(serialized._errors)


class ConfigWordsViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving ConfigWords.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 02/06/2018
    # @version 1.0.0
    > Parameters:

    * Create: POST config/words/ => **word**.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    serializer_class = WordPeligroSerializer

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos guardados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        queryset = WordPeligro.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, format=None):
        """
            Método para el registro de las palabras
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        serialized = self.serializer_class(data=request.data)
        if serialized.is_valid():
            count = WordPeligro.objects.all().count()
            if count == 0:
                words = None
            elif count > 1:
                WordPeligro.objects.all().delete()
                words = None
            else:
                words = WordPeligro.objects.all().latest('id')
                words = words.id

            if words is not None:
                WordPeligro.objects(id=words).upsert_one(word=serialized.data['word'])
                return Response(serialized.data)
            else:
                word_new = WordPeligro(word=serialized.data['word'])
                word_new.save()
                return Response(serialized.data)

            return Response(serialized.data)
        else:
            return Response(serialized._errors)
