"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package config.models
#
# Modelos de la aplicación de configuraciòn
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from django_mongoengine import Document, fields

class CredentialTwitter(Document):
    """
        Class  que construye el modelo de datos de los CredentialTwitter
    """
    # Campo de owner que indica el nombre del dueño de las credenciales
    consumer_key = fields.StringField(max_length=28)
    consumer_secret = fields.StringField(max_length=56)
    access_token = fields.StringField(max_length=56)
    access_token_secret = fields.StringField(max_length=50)
    is_active = fields.BooleanField(default=True)

    @classmethod
    def create_credential(cls, consumer_key,
                                        consumer_secret, access_token,
                                        access_token_secret):
        credential = cls(consumer_key=consumer_key,
                                    consumer_secret=consumer_secret,
                                    access_token=access_token,
                                    access_token_secret=access_token_secret)
        credential.save()
        return credential


class CredentialFacebook(Document):
    """
        Class  que construye el modelo de datos de los CredentialTwitter
    """
    # Campo de owner que indica el nombre del dueño de las credenciales

    email_phone = fields.StringField(max_length=128)
    password = fields.StringField(max_length=128,
                                            verbose_name='password')
    is_active = fields.BooleanField(default=True)

    @classmethod
    def create_credential(cls, email_phone,
                                        password):
        if email_phone is not None:
            try:
                email_name, domain_part = email_phone.strip().split('@', 1)
            except ValueError:
                pass
            else:
                email_phone = '@'.join([email_name, domain_part.lower()])

        credential = cls(email_phone=email_phone,
                                    password=password)
        credential.save()
        return credential


class WordPeligro(Document):
    """
        Clase que construye el modelo de datos para almacenar las palabras de configuracion para analizar el peligro de los user
    """
    word = fields.StringField()
