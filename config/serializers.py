"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package config.serializers
#
# Serializer de la aplicación de configuración
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from rest_framework_mongoengine.serializers import DocumentSerializer
from .models import *

class CredentialTwitterSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los CredentialTwitter
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = CredentialTwitter
        fields = '__all__'


class CredentialFacebookSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los CredentialFacebook
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = CredentialFacebook
        fields = '__all__'


class WordPeligroSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los de las palabra peligrosas
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = WordPeligro
        fields = '__all__'
