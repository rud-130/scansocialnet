"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package facebook_api.views
#
# Routers de los viewset de la aplicaciones rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0
from datetime import datetime
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest.models import *
from config.models import CredentialFacebook
from .facebook_sdk import FacebookScraping
from .functions import convert_date, facebook_friends, iterate_over_html_tags
from scrapy.selector import Selector
from users.authentication import TokenAuthentication
import requests, re


class FacebookGetGeneralData(APIView):
	"""
		Class para obtener los datos de una cuenta en base al token
	"""
	permission_classes = (permissions.IsAuthenticated, )
	authentication_classes = (TokenAuthentication, )

	def post(self, request, format=None):
		"""
			Método de la petición por post
			@param self Objeto que instancia el método
			@param request Objeto con la petición
			@param format Formato de la respuesta (json por omisión)
			@return Retorna los datos y almacena en base de datos
		"""
		account = request.data.get('account')
		if account:
			try:
				fb = CredentialFacebook.objects(is_active=True).first()
				facebook = FacebookScraping(fb.email_phone,fb.password)
				session = requests.session()
				try:
					cookies = facebook.login(session)
				except AssertionError:
					return Response({'success':False,'message':'Credenciales de facebook inválidas'})
				personal_data = {}
				## Personal Info
				PROTECTED_URL = 'https://m.facebook.com/'+account+'/about/'
				personal_info = session.get(PROTECTED_URL, cookies=cookies, allow_redirects=False)
				if personal_info.status_code == 404:
					return Response({'success':False,'message':'El usuario solicitado no existe'})
				title = Selector(text=personal_info.text).css('head title::text').extract_first()
				if(not title or 'no encontrada' in title or 'not found' in title):
					return Response({'success':False,'message':'perfil no encontrado'})
				name = Selector(text=personal_info.text).css('#root strong::text').extract_first()
				personal_data['user'] = account
				personal_data['username'] = name
				profile_picture = Selector(text=personal_info.text).css('#root .bm a img::attr(src)').extract()[0]
				personal_data['profile_picture'] = profile_picture
				user = UserSocial.objects(id_user=account).upsert_one(username=name,tipo_social='facebook',
					img=profile_picture)
				## Work
				work  = iterate_over_html_tags(Selector(text=personal_info.text).css('#work > div > div:nth-child(2) > div').extract(),'<br/>',2)
				personal_data['works'] = work
				## Education
				education = Selector(text=personal_info.text).css('#education > div > div:nth-child(2) a::text').extract()
				personal_data['education'] = education
				## Living
				living = Selector(text=personal_info.text).css('#living a::text').extract()
				personal_data['living'] = living
				## Basic Info
				basici = Selector(text=personal_info.text).css('#basic-info > div > div:nth-child(2) tr span::text').extract()
				basici_desc = Selector(text=personal_info.text).css('#basic-info > div > div:nth-child(2) tr div').extract()
				basici_desc = iterate_over_html_tags(basici_desc)
				basic_info = []
				for i in range(len(basici)):
					basic_info.append({'basic':basici[i],'description':basici_desc[(i*2)+1]})
				personal_data['basic_info'] = basic_info
				## Family
				family = Selector(text=personal_info.text).css('#family img::attr(alt)').extract()
				personal_data['family'] = family
				return Response({'success':True,'data':personal_data})
			except Exception as e:
				return Response({'success':False,'message':str(e)})
		return Response({'success':False,'message':'No envió la cuenta'})


class FacebookGetFriendData(APIView):
	"""
		Class para obtener los amigos de una cuenta pasada por parametro
	"""
	permission_classes = (permissions.IsAuthenticated, )
	authentication_classes = (TokenAuthentication, )

	def post(self, request, format=None):
		"""
			Método de la petición por post
			@param self Objeto que instancia el método
			@param request Objeto con la petición
			@param format Formato de la respuesta (json por omisión)
			@return Retorna los datos y almacena en base de datos
		"""
		account = request.data.get('account')
		fb = CredentialFacebook.objects(is_active=True).first()
		facebook = FacebookScraping(fb.email_phone,fb.password)
		session = requests.session()
		try:
			cookies = facebook.login(session)
		except AssertionError:
			return Response({'success':False,'message':'Credenciales de facebook inválidas'})
		try:
			user = UserSocial.objects(id_user=account).get()
		except Exception as e:
			return Response({'success':False,'message':str(e)})
		if account and user:
			try:
				friends = facebook_friends(account,session,cookies,user)
				data = 0
				if(friends):
					data = UserContact.objects(user=user).count()
				else:
					return Response({'success':False,'message':'No se consiguieron amigos'})
				return Response({'success':friends,'data':data})
			except Exception as e:
				return Response({'success':False,'message':str(e)})
		return Response({'success':False,'message':'No envío todos los parámetros solicitados'})

class FacebookGetPostData(APIView):
	"""
		Class para obtener los post y reacciones de una cuenta pasada por parametro
	"""
	#permission_classes = (permissions.IsAuthenticated, )
	#authentication_classes = (TokenAuthentication, )

	def post(self, request, format=None):
		"""
			Método de la petición por post
			@param self Objeto que instancia el método
			@param request Objeto con la petición
			@param format Formato de la respuesta (json por omisión)
			@return Retorna los datos y almacena en base de datos
		"""
		account = request.data.get('account')
		init_date = request.data.get('init_date')
		end_date = request.data.get('end_date')
		fb = CredentialFacebook.objects(is_active=True).first()
		facebook = FacebookScraping(fb.email_phone,fb.password)
		session = requests.session()
		try:
			cookies = facebook.login(session)
		except AssertionError:
			return Response({'success':False,'message':'Credenciales de facebook inválidas'})
		try:
			user = UserSocial.objects(id_user=account).get()
		except Exception as e:
			return Response({'success':False,'message':str(e)})
		if account and user and init_date and end_date:
			try:
				try:
					init_date = convert_date(init_date,'%d/%m/%Y')
					end_date = convert_date(end_date,'%d/%m/%Y')
				except:
					return Response({'success':False})	
				## Post Reactions
				POST_URL = 'https://m.facebook.com/'+account+'?v=timeline'
				year = []
				current_year = []
				more_posts = True
				while(more_posts):
					post_info = session.get(POST_URL, cookies=cookies, allow_redirects=False)
					posts = Selector(text=post_info.text).css('#structured_composer_async_container > div > div > div > div').extract()
					for post in posts:
						post_title = Selector(text=post).css('div:nth-child(1) div:nth-child(1) h3').extract_first()
						if post_title:
							post_title = re.sub(r'<.*?>', '',post_title)
						post_content = Selector(text=post).css('div:nth-child(1) div:nth-child(2) div:nth-child(2) div div:nth-child(2) div:nth-child(2)').extract_first()
						if post_content:
							post_content = re.sub(r'<.*?>', '',post_content)
						href = Selector(text=post).css('span[id^=like] a:nth-child(1)::attr(href)').extract_first()
						if(href):
							if 'story.php' in href:
								post_id = href.split('?')[1].split('&')[0].replace('story_fbid=','')
							elif 'photo.php' in href:
								post_id = href.split('?')[1].split('&')[0].replace('fbid=','')
							data = session.get('https://m.facebook.com'+href, cookies=cookies, allow_redirects=False)
							if 'story.php' in href:
								date = Selector(text=data.text).css('#m_story_permalink_view abbr::text').extract_first()
							elif 'photo.php' in href:
								date = Selector(text=data.text).css('#MPhotoContent abbr::text').extract_first()
							try:
								date = convert_date(date,'%d de %m a las %X',':00')
								date = date.replace(year=datetime.now().year)
							except:
								try:
									if 'a las' in date:
										date = convert_date(date,'%d de %m de %Y a las %X',':00')
									else:
										date = convert_date(date,'%d de %m de %Y')
								except:
									try:
										date = convert_date(date,'%d de %m')
										date = date.replace(year=datetime.now().year)
									except:
										date = datetime.now()
							if(date<init_date):
								more_posts = False
								break
							if(date>init_date and date<end_date):
								# Se guarda el post
								post_model = Post.objects(fecha_creacion=date,
									id_post=post_id,user=user).upsert_one(fecha_creacion=date,
									id_post=post_id,user=user, shares = 0, reaction = 0, 
									historia = post_title, mensaje=post_content)
								if 'story.php' in href:
									a_detail = Selector(text=data.text).css('#m_story_permalink_view div:nth-child(3) a::attr(href)').extract()[0]
								if 'photo.php' in href:
									a_detail = Selector(text=data.text).css('#MPhotoContent div:nth-child(2) div div div:nth-child(2) a::attr(href)').extract()[0]
								if 'https://lm.facebook.com/' not in a_detail:
									reactions = session.get('https://m.facebook.com'+a_detail, cookies=cookies, allow_redirects=False)
									#likes
									likes = Selector(text=reactions.text).css('#objects_container ul a::text').extract()
									#Likes saves
									for like in likes:
										like_model = Reaction.objects(post=post_model,author=like).upsert_one(post=post_model,author=like)
									post_model.reaction = len(likes)
								else:
									post_model.reaction = 0
								post_model.save()
					next_url = Selector(text=post_info.text).css('#structured_composer_async_container > div:nth-child(2) a::attr(href)').extract()
					if len(next_url)>0:
						next_text = Selector(text=post_info.text).css('#structured_composer_async_container > div:nth-child(2) a::text').extract()
						if('Mostrar' not in next_text[0]):
							j = 0
							for link in next_text:
								if(link not in current_year):
									current_year.append(link)
									break
								j +=1
							if(j<len(next_url)):
								next_url = [next_url[j]]
							else:
								more_posts = False
						POST_URL = 'https://m.facebook.com'+next_url[0]
					else:
						more_posts = False
				data = Post.objects(user=user).count()
				return Response({'success':True,'data':data})
			except Exception as e:
				return Response({'success':False,'message':str(e)})
		return Response({'success':False,'message':'No envío todos los parámetros solicitados'})


class FacebookGetPhotoData(APIView):
	"""
		Class para obtener las fotos de una cuenta pasada por parametro
	"""
	permission_classes = (permissions.IsAuthenticated, )
	authentication_classes = (TokenAuthentication, )

	def post(self, request, format=None):
		"""
			Método de la petición por post
			@param self Objeto que instancia el método
			@param request Objeto con la petición
			@param format Formato de la respuesta (json por omisión)
			@return Retorna los datos y almacena en base de datos
		"""
		account = request.data.get('account')
		fb = CredentialFacebook.objects(is_active=True).first()
		facebook = FacebookScraping(fb.email_phone,fb.password)
		session = requests.session()
		try:
			cookies = facebook.login(session)
		except AssertionError:
			return Response({'success':False,'message':'Credenciales de facebook inválidas'})
		try:
			user = UserSocial.objects(id_user=account).get()
		except Exception as e:
			return Response({'success':False,'message':str(e)})
		if account and user:
			try:
				## Photo Info
				GALLERY_URL = 'https://m.facebook.com/'+account+'/photos'
				photo_info = session.get(GALLERY_URL, cookies=cookies, allow_redirects=False)
				albums = Selector(text=photo_info.text).css('#objects_container div:nth-child(2) a::attr(href)').extract()
				for album in albums:
					if 'photoset' in album:
						PHOTO_URL = 'https://m.facebook.com'+album
						detail_p = session.get(PHOTO_URL, cookies=cookies, allow_redirects=False)
						photos = Selector(text=detail_p.text).css('#root tbody div a::attr(href)').extract()
						photos = [p for p in photos if 'photo.php' in p ]
						for photo_url in photos:
							PHOTO_DETAIL_URL = 'https://m.facebook.com'+photo_url
							photo_detail = session.get(PHOTO_DETAIL_URL, cookies=cookies, allow_redirects=False)
							p_url = Selector(text=photo_detail.text).css('#root img::attr(src)').extract()[0]
							p_date = Selector(text=photo_detail.text).css('#MPhotoContent abbr::text').extract()[0]
							p_tags = Selector(text=photo_detail.text).css('#root img::attr(alt)').extract()
							if(len(p_tags)>0):
								p_tags = p_tags[0]
							else:
								p_tags = ''
							#Photo save
							if len(p_date)<=15:
								from datetime import datetime
								dates = datetime.now()
							else:
								dates = convert_date(p_date,'%d de %m de %Y')
							photo_model = Photo.objects(
								url=p_url, fecha_creacion=dates,user=user).upsert_one(
								url=p_url, fecha_creacion=dates,user=user, tags=p_tags)
						next_buttom = Selector(text=detail_p.text).css('#m_more_item a::attr(href)').extract()
						if len(next_buttom)>0:
							PHOTO_URL = 'https://m.facebook.com'+next_buttom[0]
						else:
							break
				data = Photo.objects(user=user).count()
				return Response({'success':True,'data':data})
			except Exception as e:
				return Response({'success':False,'message':str(e)})
		return Response({'success':False,'message':'No envío todos los parámetros solicitados'})
