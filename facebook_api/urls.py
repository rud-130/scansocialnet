"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package facebook_api.url
#
# Urls de la aplicación
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^general/$', FacebookGetGeneralData.as_view(),name='facebook_general'),
	url(r'^friends/$', FacebookGetFriendData.as_view(),name='facebook_friends'),
	url(r'^posts/$', FacebookGetPostData.as_view(),name='facebook_posts'),
	url(r'^photos/$', FacebookGetPhotoData.as_view(),name='facebook_photos'),
]