"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package facebook_api.functions
#
# Functions funciones  que seran usadas en las vistas
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0
import re
from datetime import datetime
from scrapy.selector import Selector
from rest.models import *
from .constantes import MESES


def convert_date(date,format,extra=''):
    """
    Funcion para convertir str a fecha

    @param date Recibe la fecha a convertir
    @param format Recibe el formato de la fecha
    @param extra Recibe valor extra para agregar al final
    @return date Retorna la fecha como objeto datetime
    """
    for i in range(0,len(MESES)):
        if(date.find(MESES[i])!=-1):
            month = i+1
            if((i+1)<10):
                month = str(i+1).zfill(2)
            date = date.replace(MESES[i],str(month))
            date += extra
            break
    return datetime.strptime(date,format)


def facebook_friends(account,session,cookies,user,deep=2):
    """
    Funcion para extraer amigos

    @param account Recibe la cuenta
    @param session Recibe la sesión
    @param cookies Recibe las cookies
    @param user Recibe el usuario
    @param deep Recibe la profundidad
    @return Retorna Verdadero si se ejecutó la función
    """
    FRIENDS_URL = 'https://m.facebook.com/'+account+'/friends/'
    while(True):
        friends_info = session.get(FRIENDS_URL, cookies=cookies, allow_redirects=False)
        if friends_info.status_code != 200:
            return False
        #friends = Selector(text=friends_info.text).css('#root > div > div td a::text').extract()
        friends = Selector(text=friends_info.text).css('#root > div > div > div td > a')
        # Friend Saves
        for friend_css in friends:
            friend = friend_css.css('a::text').extract()[0]
            #if(deep==2 and 'Agregar a amigos' not in friend and 'Más' not in friend and 'Mensaje' not in friend):
            #    facebook_friends(friend,session,cookies,1)
            friend_uri = friend_css.css('a::attr(href)').extract()[0] if len(friend_css.css('a::attr(href)').extract())>0 else ''
            if 'Agregar a amigos' not in friend and 'Más' not in friend and 'Mensaje' not in friend and friend_uri:
                friend_model = Contact.objects(name=friend,url=friend_uri).upsert_one(
                    name=friend,url=friend_uri)
                friend_inter = UserContact.objects(user=user,contact=friend_model).upsert_one(
                    user=user,contact=friend_model)
                # Se genera un nuevo usuario
                acc = friend_uri.split('?fref')[0][1:]
                if 'profile.php' in acc:
                    acc = acc.split('&')[0]
                friend_user = UserSocial.objects(id_user=acc).upsert_one(username=friend,tipo_social='facebook')
                #if 'profile.php' not in acc and deep==2:
                #    facebook_friends(acc,session,cookies,friend_user,1)
        more_friends = Selector(text=friends_info.text).css('#m_more_friends a::attr(href)').extract()
        if len(more_friends)>0:
            FRIENDS_URL = 'https://m.facebook.com'+more_friends[0]
        else:
            break
        if len(friends)==0:
            break
    return True

def iterate_over_html_tags(string_list,sub_string='',option=1):
    """
    Funcion para remover etiquetas html

    @param string_list Recibe una lista de etiquetas html
    @param sub_string Recibe el substring para reemplazar
    @param option Recibe la opción a ejecutar
    @return Retorna lista de etiquetas html
    """
    new_list = []
    for item in string_list:
        if(option==1):
            new_list.append(remove_html_tags(item,sub_string))
        elif (option==2):
            new_list.append(split_html_tags(item,sub_string))
    return new_list

def remove_html_tags(string,sub_string=''):
    """
    Funcion para remover etiquetas html

    @param string Recibe el string a remover
    @param sub_string Recibe el substring para reemplazar
    @return Retorna el string sin etiquetas html
    """
    regex = re.compile(r'<.*?>')
    return regex.sub(sub_string, string)

def split_html_tags(string, sub_string=' '):
    """
    Funcion para remover y separar etiquetas html

    @param string Recibe el string a remover
    @param sub_string Recibe el substring para unir
    @return Retorna el string sin etiquetas html
    """
    split_list = re.split(r'<.*?>',string)
    new_str = [item for item in split_list if item!='']
    return sub_string.join(new_str)