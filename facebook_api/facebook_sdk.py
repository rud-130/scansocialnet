"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package facebook_api.views
#
# Routers de los viewset de la aplicaciones rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

class FacebookScraping():
	"""
	Class para hacer scraping en facebook
	"""

	def __init__(self, email, password):
		"""
		Método inicial para clase

		@param self Objeto que instancia el método
		@param email Dirección de Correo
		@param password Contraseña de facebook
		"""
		self.email = email
		self.password = password

	def login(self, session):
		"""
		Método para logearse

		@param self Objeto que instancia el método
		@param session Se pasa la sesión
		@return Retorna las cookies de la sesión
		"""

		# Attempt to login to Facebook
		response = session.post('https://m.facebook.com/login.php', data={
		'email': self.email,
		'pass': self.password
		}, allow_redirects=False)

		assert response.status_code == 302
		assert 'c_user' in response.cookies
		return response.cookies

if __name__ == "__main__":

    session = requests.session()
    cookies = login(session, USERNAME, PASSWORD)
    response = session.get(PROTECTED_URL, cookies=cookies, allow_redirects=False)