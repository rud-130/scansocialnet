from nltk.corpus import stopwords

class DangerSdk():
	"""!
	Sdk para determinar si un usuario es peligroso

	@date 03-06-2018
	@version 1.0.0
	"""

	def clean(self,word):
		"""
			Método para limpiar una palabra
			@param self Objeto que instancia el método
			@param word Palabra para ser limpiada
			@return Retorna la palabra limpia
		"""
		clean_word = word.lower().replace(',','').replace(';','').replace(':','').replace('.','').replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u')
		return clean_word

	def remove_stopwords(self,posts):
		"""
			Método para remover las stopwords
			@param self Objeto que instancia el método
			@param posts Recibe el listado de posts
			@return Retorna los post sin palabras innecesarias
		"""
		new_posts = []
		for post in posts:
			new_post = []
			words = post.split(" ")
			for word in words:
				if(word not in stopwords.words('spanish')):
					new_post.append(self.clean(word))
			new_posts.append(" ".join(new_post))
		return new_posts


	def danger_percent(self,posts,danger_list):
		"""
			Método para limpiar una palabra
			@param self Objeto que instancia el método
			@param posts Recibe el listado de posts
			@param danger_list Recibe el listado de palabras de peligro
			@return Retorna el porcentaje de peligro
		"""
		danger_dict = {}
		word_q = 0
		danger_q = 0
		for danger in danger_list:
			danger_dict[danger] = 0
		for post in posts:
			words = post.split(" ")
			word_q += len(words)
			for word in words:
				for danger_word in danger_list:
					if word.find(danger_word)!=-1:
						danger_dict[danger_word] += 1
						danger_q += 1
		if(word_q==0):
			d_percent = 0
		else:	
			d_percent = (danger_q/float(word_q))*100
		return {'danger_percent':d_percent,'words_dict':danger_dict}
