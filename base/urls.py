"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package base.url
#
# Urls de la aplicación
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from django.conf.urls import url
from .views import *
from users.views import *

urlpatterns = [
	url(r'^$', InicioView.as_view(), name = "inicio"),
	url(r'^usuarios/crear$', RegisterView.as_view(), name = "user_create"),
	url(r'^usuarios/lista$', UserListView.as_view(), name = "user_list"),
	url(r'^usuarios/actualizar/(?P<pk>.+)$', UserUpdateView.as_view(), name = "user_edit"),
	url(r'^usuarios/borrar/(?P<pk>.+)$', UserDeleteView.as_view(), name = "user_delete"),
	url(r'^login$', LoginView.as_view(), name = "login"),
    url(r'^logout$', LogoutView.as_view(), name = "logout"),
]
