"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package base.views
#
# Vistas de la aplicación
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import (
    FormView, DeleteView, CreateView, 
    UpdateView, ListView, TemplateView
    )
from users.models import User
from users.forms import UserUpdateForm, UserRegisterForm

class InicioView(LoginRequiredMixin,TemplateView):
	"""!
	Clase que gestiona la vista principal

	@date 29-04-2018
	@version 1.0.0
	"""
	template_name = 'inicio.html'

class UserListView(LoginRequiredMixin,ListView):
	"""!
	Clase que gestiona el listado de usuarios

	@date 01-05-2018
	@version 1.0.0
	"""
	model = User
	template_name = 'user.list.html'
	paginate_by = 10

class UserUpdateView(LoginRequiredMixin,SuccessMessageMixin,UpdateView):
    """!
    Clase que gestiona la actualización de usuarios

    @date 01-05-2018
    @version 1.0.0
    """
    model = User
    form_class = UserUpdateForm
    template_name = 'user.update.html'
    success_url = reverse_lazy('user_list')
    success_message = "Se actualizó el usuario con éxito"

    def form_valid(self,form):
        """!
        Metodo que valida si el formulario es valido

        @date 26-05-2018
        @param self <b>{object}</b> Objeto que instancia la clase
        @param form <b>{object}</b> Objeto que contiene el formulario de registro
        @return Retorna el formulario validado
        """

        self.object = form.save(commit=False)
        if(form.cleaned_data['password']):
          self.object.set_password(form.cleaned_data['password'])

        return super(UserUpdateView, self).form_valid(form)


class UserDeleteView(SuccessMessageMixin, LoginRequiredMixin,DeleteView):
	"""!
	Clase que gestiona el borrado de usuarios

	@date 01-05-2018
	@version 1.0.0
	"""
	model = User
	template_name = "user.delete.html"
	success_message = "Se eliminó el usuario con éxito"
	success_url = reverse_lazy('user_list')

	def delete(self, request, *args, **kwargs):
		"""
		Método para el registro de usuarios
		@param self Objeto que instancia el método
		@param request Objeto con la petición
		@param args Argumentos pasados a la vista
		@param kwargs Objetos pasados a la vista
		@return Elimina el objeto
		"""
		messages.success(self.request, self.success_message)
		return super(UserDeleteView, self).delete(request, *args, **kwargs)

class RegisterView(SuccessMessageMixin,CreateView):
	"""!
	Muestra el formulario de registro de usuarios

	@date 02-05-2018
	@version 1.0.0
	"""
	template_name = "user.register.html"
	form_class = UserRegisterForm
	success_url = reverse_lazy('inicio')
	success_message = "Se registró con éxito"
	model = User																	