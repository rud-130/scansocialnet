"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package twitter.views
#
# Views de las vista genericas de la aplicacion rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from mongoengine import *


class UserSocial(Document):
    """
        Class  que construye el modelo de datos de los Usuarios
    """
    # identificador del usuario en la red social
    id_user = StringField(required=True, max_length=56, unique=True)
    # tipo de red social de usuario
    tipo_social = StringField(required=True, max_length=12)
    # nombre del usuario en la red social
    username = StringField(required=True, max_length=56)
    # campo que refiere a la imagen de la cuenta
    img = StringField(required=False)


class Post(Document):
    """
        Class  que construye el modelo de datos de los posts
    """
    # identificador del post
    id_post = StringField(required=True, max_length=56, unique=True)
    # Campo de refrencia al modelo UserSocial
    user = ReferenceField(UserSocial)
    # campo que refiere a la historia en facebook o un retwe
    historia = StringField(required=False)
    # campo que refiere al post de la red social
    mensaje = StringField(required=False)
    # fecha de creaciondel post
    fecha_creacion = DateTimeField()
    # campo que refiere a la imagen del post
    img = StringField(required=False)
    # campo que refiere al contador del compartir y los retweet del post
    shares = IntField()
    # campo que refiere al contador del las reacciones del post
    reaction = IntField()


class Comment(Document):
    """
        Class  que construye el modelo de datos de los comentarios
    """
    # identificador de comentario
    id_comment = StringField(required=True, max_length=128, unique=True)
    # campo de referencia al modelo post
    post = ReferenceField(Post)
    # mensaje del comentario
    mensaje = StringField(required=True)
    # autor que realiza el comentario
    author = StringField(required=True, max_length=56)
    # fecha en la que se creo el comentario
    fecha_creacion = DateTimeField()


class Reaction(Document):
    """
        Class  que construye el modelo de datos de las reacciones
    """
    # campo de referencia al modelo post
    post = ReferenceField(Post)
    # autor de la reaccion
    author = StringField(required=True, max_length=56)


class Photo(Document):
    """
        Class  que construye el modelo de datos de las fotos
    """
    # campo que refiere a la url de la imagen del post
    url = StringField()
    # fecha de creaciondel post
    fecha_creacion = DateTimeField()
    # campo que refiere a las etiquetas de la imagen
    tags = StringField()
    # Campo de refrencia al modelo UserSocial
    user = ReferenceField(UserSocial)


class Contact(Document):
    """
        Class  que construye el modelo de datos de los contactos
    """
    # Campo tipo de contacto
    tipo = StringField(required=True, max_length=50, default="Amigo")
    # Campo nombre que define el nombre del contacto
    name = StringField(required=True)
    # Campo url que define la direccion del perfil
    url = StringField(required=True)


class UserContact(Document):
    """
        Class  que construye el modelo de datos de los UserContact
    """
    # Campo de referencia al modelo UserSocial
    user = ReferenceField(UserSocial)
    # Cmpao de referencia al model Contact
    contact = ReferenceField(Contact)

    class Meta:
        unique_together = (("user", "contact"),)
