"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package rest.routers
#
# Routers de los viewset de la aplicaciones rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from rest_framework.routers import DefaultRouter

from .views import *
from users.views import *
from twitter.views import *
from config.views import *


router = DefaultRouter()

#Router Credential
router.register(r'credential/twitter', CredentialTwitterViewSet, 'credential_twitter')
router.register(r'credential/facebook', CredentialFacebookViewSet, 'credential_facebook')

# Router Config words
router.register(r'config/words', ConfigWordsViewSet, 'config_word')

router.register(r'social_scan/users', UserSocialViewSet, 'users_social')
router.register(r'social_scan/posts', PostViewSet, 'posts_social')
router.register(r'social_scan/comments', CommentViewSet, 'comments_social')
router.register(r'social_scan/photos', PhotoViewSet, 'photos_social')
router.register(r'social_scan/contacts', ContactViewSet, 'contacts_social')
router.register(r'social_scan/reactions', ReactionViewSet, 'reaction_social')
router.register(r'social_scan/users-contacts', UserContactViewSet, 'user_contacts_social')
router.register(r'social_scan/users-graph', ContacGraphView, 'user_contacts_graph')

# Routers Facebook
router.register(r'facebook/reactions', PostGroupViewSet, 'facebook_reactions')

router.register(r'post/date', PostDateGroupViewSet, 'post_date')
router.register(r'danger_words', DangerViewSet, 'danger_words')

# Routers Twitter
router.register(r'twitter', TwitterUserView, 'twitter')
router.register(r'twitter/posts', TwitterPostView, 'twitter_posts')
router.register(r'twitter/contacts/follower', TwitterFollower, 'twitter_follower')
router.register(r'twitter/contacts/following', TwitterFollowing, 'twitter_following')
router.register(r'twitter/contacts',
                TwitterFollowerFollowingView, 'twitter_contacts')

# Routers Users
router.register(r'user', UserViewSet, "user")
router.register(r'register/user', RegisterUser, 'register')
