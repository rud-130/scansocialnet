"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package rest.views
#
# Views de las vista genericas de la aplicacion rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0
import time

from datetime import datetime, date

from rest_framework import viewsets, permissions
from rest_framework.response import Response

from .models import *
from .serializers import *
from users.authentication import TokenAuthentication
from config.models import WordPeligro
from facebook_api.functions import convert_date
from base.danger_sdk import DangerSdk

tupla_diassem = ("Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom")


class UserSocialViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving Users.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

      * Create: POST twitter/users/
      * Consult All: GET twitter/users/
      * Consult One: GET twitter/users/ID.
      * Update: PATCH or PUT twitter/users/ID
      * Delete: DELETE twitter/users/ID.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    model = UserSocial
    queryset = UserSocial.objects.all()
    serializer_class = UserSerializer
    my_filter_fields = ('username', 'tipo_social','id_user') # specify the fields on which you want to filter

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {}
        for field in  self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value:
                filtering_kwargs[field] = field_value
        return filtering_kwargs

    def get_queryset(self):
        queryset = self.model.objects.all()
        filtering_kwargs = self.get_kwargs_for_filtering() # get the fields with values for filtering 
        if filtering_kwargs:
            queryset = self.model.objects.filter(**filtering_kwargs) # filter the queryset based on 'filtering_kwargs'
        return queryset


class PostViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving Posts.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

      * Create: POST twitter/posts/
      * Consult All: GET twitter/posts/
      * Consult One: GET twitter/posts/ID.
      * Update: PATCH or PUT twitter/posts/ID
      * Delete: DELETE twitter/posts/ID.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    model = Post
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    my_filter_fields = ('fecha_creacion', 'username', 'id_user') # specify the fields on which you want to filter

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {}
        for field in  self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value:
                filtering_kwargs[field] = field_value
        return filtering_kwargs

    def get_queryset(self):
        queryset = self.model.objects.all()
        filtering_kwargs = self.get_kwargs_for_filtering() # get the fields with values for filtering 
        if filtering_kwargs:
            if  'username' in filtering_kwargs:
                user = UserSocial.objects(username=filtering_kwargs['username'])
                queryset = self.model.objects.filter(user__in=user)
            elif 'id_user' in filtering_kwargs:
                user = UserSocial.objects(id_user=filtering_kwargs['id_user'])
                queryset = self.model.objects.filter(user__in=user)
            else:
                queryset = self.model.objects.filter(**filtering_kwargs) # filter the queryset based on 'filtering_kwargs'
        serializer = self.get_serializer(queryset, many=True)
        return queryset



class CommentViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving Comments.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

      * Create: POST twitter/comments/
      * Consult All: GET twitter/comments/
      * Consult One: GET twitter/comments/ID.
      * Update: PATCH or PUT twitter/comments/ID
      * Delete: DELETE twitter/comments/ID.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    model = Comment
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    my_filter_fields = ('id_post',) # specify the fields on which you want to filter

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {}
        for field in  self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value:
                filtering_kwargs[field] = field_value
        return filtering_kwargs

    def get_queryset(self):
        queryset = self.model.objects.all()
        filtering_kwargs = self.get_kwargs_for_filtering() # get the fields with values for filtering 
        if filtering_kwargs:
            if  filtering_kwargs['id_post'] is not None:
                post = Post.objects(id_post=filtering_kwargs['id_post'])
                queryset = self.model.objects.filter(post__in=post)
            else:
                queryset = self.model.objects.filter(**filtering_kwargs) # filter the queryset based on 'filtering_kwargs'
        return queryset


class ReactionViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving Reactions.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

      * Create: POST twitter/reactions/
      * Consult All: GET twitter/reactions/
      * Consult One: GET twitter/reactions/ID.
      * Update: PATCH or PUT twitter/reactions/ID
      * Delete: DELETE twitter/reactions/ID.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    model = Reaction
    queryset = Reaction.objects.all()
    serializer_class = ReactionSerializer
    my_filter_fields = ('id_post',) # specify the fields on which you want to filter

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {}
        for field in  self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value:
                filtering_kwargs[field] = field_value
        return filtering_kwargs

    def get_queryset(self):
        queryset = self.model.objects.all()
        filtering_kwargs = self.get_kwargs_for_filtering() # get the fields with values for filtering 
        if filtering_kwargs:
            if  filtering_kwargs['id_post'] is not None:
                post = Post.objects(id_post=filtering_kwargs['id_post'])
                queryset = self.model.objects.filter(post__in=post)
            else:
                queryset = self.model.objects.filter(**filtering_kwargs) # filter the queryset based on 'filtering_kwargs'
        return queryset


class PhotoViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving Photos.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

      * Create: POST twitter/photos/
      * Consult All: GET twitter/photos/
      * Consult One: GET twitter/photos/ID.
      * Update: PATCH or PUT twitter/photos/ID
      * Delete: DELETE twitter/photos/ID.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    model = Photo
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    my_filter_fields = ('fecha_creacion', 'username', 'id_user') # specify the fields on which you want to filter

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {}
        for field in  self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value:
                filtering_kwargs[field] = field_value
        return filtering_kwargs

    def get_queryset(self):
        queryset = self.model.objects.all()
        filtering_kwargs = self.get_kwargs_for_filtering() # get the fields with values for filtering 
        if filtering_kwargs:
            if  'username' in filtering_kwargs:
                user = UserSocial.objects(username=filtering_kwargs['username'])
                queryset = self.model.objects.filter(user__in=user)
            elif 'id_user' in filtering_kwargs:
                user = UserSocial.objects(id_user=filtering_kwargs['id_user'])
                queryset = self.model.objects.filter(user__in=user)
            else:
                queryset = self.model.objects.filter(**filtering_kwargs) # filter the queryset based on 'filtering_kwargs'
        return queryset


class ContactViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving Contacts.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

      * Create: POST twitter/contact/
      * Consult All: GET twitter/contact/
      * Consult One: GET twitter/contact/ID.
      * Update: PATCH or PUT twitter/contact/ID
      * Delete: DELETE twitter/contact/ID.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    model = Contact
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    my_filter_fields = ('name', 'tipo') # specify the fields on which you want to filter

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {}
        for field in  self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value:
                filtering_kwargs[field] = field_value
        return filtering_kwargs

    def get_queryset(self):
        queryset = self.model.objects.all()
        filtering_kwargs = self.get_kwargs_for_filtering() # get the fields with values for filtering 
        if filtering_kwargs:
            queryset = self.model.objects.filter(**filtering_kwargs) # filter the queryset based on 'filtering_kwargs'
        return queryset


class UserContactViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving UserContact.

    # @author Leonel Hernández (leonelphm at gmail)
    # @author Rodrigo Boet (rudmanmrrod at gmail)
    # @Date 06/03/2018
    # @version 1.0.0
    > Parameters:

      * Create: POST twitter/users-contacts/
      * Consult All: GET twitter/users-contacts/
      * Consult One: GET twitter/users-contacts/ID.
      * Update: PATCH or PUT twitter/users-contacts/ID
      * Delete: DELETE twitter/users-contacts/ID.
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    model = UserContact
    queryset = UserContact.objects.all()
    serializer_class = UserContactSerializer
    my_filter_fields = ('user', 'contact') # specify the fields on which you want to filter

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {}
        for field in  self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value:
                filtering_kwargs[field] = field_value
        return filtering_kwargs

    def get_queryset(self):
        queryset = self.model.objects.all()
        filtering_kwargs = self.get_kwargs_for_filtering() # get the fields with values for filtering 
        if filtering_kwargs:
          if  'user' in filtering_kwargs:
                user = UserSocial.objects(username=filtering_kwargs['user'])
                if len(user) == 0:
                    user = UserSocial.objects(id_user=filtering_kwargs['user'])
                queryset = self.model.objects.filter(user__in=user)
          elif  'contact' in filtering_kwargs:
                user = Contact.objects(name=filtering_kwargs['contact'])
                queryset = self.model.objects.filter(contact__in=user)
          else:
             queryset = self.model.objects.filter(**filtering_kwargs) # filter the queryset based on 'filtering_kwargs'
        return queryset


class ContacGraphView(viewsets.ViewSet):
    """
        Class costumizada para obtener los contactos de un usuario objetivo de la red Social 

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 06/03/2018
        # @version 1.0.0
        > Parameters:

          * Create: GET social_scan/users-graph
          * Consult Especific for username: GET social_scan/users-graph/?account=@usuario_objetivo ó twitter/?account=usuario_objetivo
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los contactos guardados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.GET.get('account')
        if account is not None:
            if '@' in account:
                account = account.replace('@', '')
            print(account)
            try:
                queryset = UserSocial.objects(username=account)
                if len(queryset) == 0:
                    queryset = UserSocial.objects(id_user=account)
                contact_queryset = UserContact.objects.filter(user__in=queryset)
            except Exception as e:
                print(e)
                contact_queryset =  None
                raise e

            serializer = UserContactSerializer(contact_queryset, many=True)

            return Response(serializer.data)
        else:
            return Response('Debes enviar el nombre de la cuenta, con el parametro en la url asi: twitter/?account=@nombre_usuario ó twitter/?account=nombre_usuario')


class PostGroupViewSet(viewsets.ViewSet):
    """
        Class costumizada para obtener los autores que han reaccionado a los post de un usuario objetivo de la red Social 

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 03/06/2018
        # @version 1.0.0
        > Parameters:

          * list: GET facebook/reactions
          * Consult Especific for username: GET facebook/reactions?account=usuario_objetivo 
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los autores de las reaciones guardados de un usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.GET.get('account')
        try:
            queryset = UserSocial.objects.get(id_user=account)
        except Exception as e:
            return Response({'error':str(e)})
        post_query = Post.objects.filter(user=queryset)
        reaction = []
        for post in post_query:
            reactions = Reaction.objects(post=post)
            for react in reactions:
                reaction += react.author,
        data = []
        reaction_sorted = list(set(reaction))
        for elem in reaction_sorted:
            data += {'autor': elem, "reacciones": reaction.count(elem)},
        return Response({'data': data, 'total': len(reaction)})


class PostDateGroupViewSet(viewsets.ViewSet):
    """
        Class costumizada para obtener los post de un usuario objetivo de la red Social 

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 03/06/2018
        # @version 1.0.0
        > Parameters:

          * list: GET facebook/reactions
          * Consult Especific for username: GET facebook/reactions?account=usuario_objetivo 
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los post por fecha del usaurio objetivo
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        account = request.GET.get('account', None)
        init_date = request.GET.get('init_date', None)
        end_date = request.GET.get('end_date', None)
        if account is not None and init_date is not None and end_date is not None:
            try:
                queryset = UserSocial.objects.get(username=account)
            except:
                try:
                    queryset = UserSocial.objects.get(id_user=account)
                except Exception as e:
                    return Response(str(e))
            fitro_semana_anio = []
            fechas = []
            data = {}
            start = init_date.split("/")
            end = end_date.split("/")
            
            start = date(int(start[2]), int(start[1]), int(start[0]))
            end = date(int(end[2]), int(end[1]), int(end[0]))
            post_query = Post.objects(user=queryset, fecha_creacion__gte=start,
                                      fecha_creacion__lte=end)
            for post in post_query:
                fecha = post.fecha_creacion.strftime("%d-%m-%Y")
                fechas += fecha,
            control = []
            for post in post_query:
                semana_anio = post.fecha_creacion.strftime("%W-%Y")
                n_dia = datetime.isoweekday(post.fecha_creacion)
                fecha = post.fecha_creacion.strftime("%d-%m-%Y")
                if not semana_anio in fitro_semana_anio:
                    fitro_semana_anio += semana_anio,
                    control += fecha,
                    data[semana_anio] = {"semana_anio": semana_anio,
                                 "dia_semana": tupla_diassem[n_dia-1],
                                 "fecha": fecha,
                                 "num_posts": fechas.count(fecha)
                                 },
                else:
                    if not fecha in control:
                        control += fecha,
                        data[semana_anio] += {"semana_anio": semana_anio,
                                     "dia_semana": tupla_diassem[n_dia-1],
                                     "fecha": fecha,
                                     "num_posts": fechas.count(fecha)},

                print(semana_anio, tupla_diassem[n_dia-1], post.fecha_creacion.strftime("%d-%m-%Y"))
        else:
            data = {'error': "Debe enviar los parametros correctos"}
        return Response(data)


class DangerViewSet(viewsets.ViewSet):
    """
        Clase para obtener que tan peligroso es un usuario de acuerdo a post y fotos

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 03/06/2018
        # @version 1.0.0
        > Parameters:

          * list: GET danger_words
          * GET account Nombre de Usuario
          * GET init_date Fecha Inicio
          * GET end_date Fecha Fin
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )

    def list(self, request, format=None):
        account = request.GET.get('account',None)
        if account:
            danger_words = WordPeligro.objects.first()
            if not danger_words:
                return Response({'error':'No ha configurado las palabras de peligro'})
            try:
                user = UserSocial.objects.get(id_user=account)
            except:
                try:
                    user = UserSocial.objects.get(username=account)
                except:
                    return Response([])
            post_query = Post.objects(user=user)
            post_data = []
            if(len(post_query)>0):
                post_data.append(self.analize_post(post_query))
            photo_query = Photo.objects(user=user)
            photo_data = []
            if(len(photo_data)>0):
                photo_data.append(self.analize_photo(photo_query))
            return Response({'post_data': post_data,'photo_data':photo_data})
        return Response({'error':'No se envio account'})

    def analize_post(self,posts):
        posts_list = []
        for post in posts:
            if(post.mensaje is not None and post.mensaje!=''):
                posts_list.append(post.mensaje)
        danger_words = WordPeligro.objects.first()
        danger_list = danger_words.word.split(";")
        ds = DangerSdk()
        cleaned_posts = ds.remove_stopwords(posts_list)
        return ds.danger_percent(cleaned_posts,danger_list)


    def analize_photo(self,photos):
        photo_list = []
        for photo in photos:
            if(photo.tags is not None and photo.tags!=''):
                print(photo.tags)
                photo_list.append(photo.tags)
        danger_words = WordPeligro.objects.first()
        danger_list = danger_words.word.split(";")
        ds = DangerSdk()
        cleaned_posts = ds.remove_stopwords(photo_list)
        return ds.danger_percent(cleaned_posts,danger_list)
