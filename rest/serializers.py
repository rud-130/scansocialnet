"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package rest.serializers
#
# Views de las vista genericas de la aplicacion rest
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from rest_framework_mongoengine.serializers import DocumentSerializer

from .models import *


class UserSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los usuarios
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = UserSocial
        fields = '__all__'


class PostSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los Posts
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = Post
        fields = '__all__'
        depth = 1


class CommentSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los Comments
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = Comment
        fields = '__all__'
        depth = 1


class ReactionSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los Reactions
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = Reaction
        fields = '__all__'
        depth = 1


class PhotoSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los Photos
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = Photo
        fields = '__all__'
        depth = 1


class ContactSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los Contacts
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = Contact
        fields = '__all__'
        depth = 2


class UserContactSerializer(DocumentSerializer):
    """
        Class para serializar el modelo de dato de los Users Contacts
    """

    class Meta:
        """
            Class que construye el meta dato del serializado
        """
        model = UserContact
        fields = '__all__'
        depth = 1


class PostRelatedSerializer(DocumentSerializer):

    class Meta:
        model = Post
        fields = ('user', 'historia',
                  'mensaje', 'fecha_creacion',
                  'img', 'shares', 'reaction')
        depth = 2
