import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RouterApi } from '../app.service';


@Injectable()
export class FacebookService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 getBasic(params){ 
    let url = this.routerApi.ApiFacebookGeneral();
    let header = this.routerApi.createHeader();
    
    let response = this.http.post(url,params,{headers: header});
    return response;
  }

  getFriends(params){ 
    let url = this.routerApi.ApiFacebookFriends();
    let header = this.routerApi.createHeader();
    
    let response = this.http.post(url,params,{headers: header});
    return response;
  }

  getPhotos(params){ 
    let url = this.routerApi.ApiFacebookphotos();
    let header = this.routerApi.createHeader();
    
    let response = this.http.post(url,params,{headers: header});
    return response;
  }

  getPosts(params){ 
    let url = this.routerApi.ApiFacebookPosts();
    let header = this.routerApi.createHeader();
    
    let response = this.http.post(url,params,{headers: header});
    return response;
  }


}