import { Component, OnInit } from '@angular/core';
import { FacebookService } from './facebook.service';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  providers: [FacebookService],
  styleUrls: ['./facebook.component.css']
})
export class FacebookComponent implements OnInit {
  account: String;
  user_info: any;
  loading: Boolean;
  error_msj: Boolean;
  //friends
  friend_loading: Boolean;
  friend_count: Number;
  //Photos
  photo_loading: Boolean;
  photo_count: Number;
  // Posts
  post_loading: Boolean;
  post_count: Number;
  init_date: String;
  end_date: String;
  params_datepicker: String;

  constructor(private facebookService: FacebookService) { 
    this.loading = false;
    this.friend_loading = false;
    this.photo_loading = false;
    this.error_msj = false;
    this.params_datepicker = "{\
      format: 'dd/mm/yyyy',\
      labelMonthNext: 'Mes siguiente',\
      labelMonthPrev: 'Mes anterior',\
      labelMonthSelect: 'Selecciona un mes',\
      labelYearSelect: 'Selecciona un año',\
      monthsFull: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],\
      monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],\
      weekdaysFull: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],\
      weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],\
      weekdaysLetter: [ 'D', 'L', 'M', 'Mi', 'J', 'V', 'S' ],\
      today: 'Hoy',\
      clear: 'Limpiar',\
      close: 'Ok',\
    }";
  }

  ngOnInit() {

  }

  search() {
    this.loading = true;
    this.error_msj = false;
    this.facebookService.getBasic({'account':this.account}).subscribe(response => {
      if(response['success']==true){
        this.user_info = response['data'];
        this.loading = false;
      }
      else{
        toast(response['message'],5000);
        this.loading = false;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  friend_search(){
    this.friend_loading = true;
    this.facebookService.getFriends({'account':this.account}).subscribe(response => {
      if(response['success']==true){
        this.friend_count = response['data'];
        this.friend_loading = false;
      }
      else{
        toast(response['message'],5000);
        this.friend_loading = false;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  photo_search(){
    this.photo_loading = true;
    this.facebookService.getPhotos({'account':this.account}).subscribe(response => {
      if(response['success']==true){
        this.photo_count = response['data'];
        this.photo_loading = false;
      }
      else{
        toast(response['message'],5000);
        this.photo_loading = false;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  post_search(){
    this.post_loading = true;
    if(this.init_date!='' && this.end_date!=''){
      this.facebookService.getPosts({'account':this.account,'init_date':this.init_date,
      'end_date':this.end_date}).subscribe(response => {
        if(response['success']==true){
          this.post_count = response['data'];
          this.post_loading = false;
        }
        else{
          toast(response['message'],5000);
          this.post_loading = false;
        }
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
    }
  }

}
