import { Component, OnInit } from '@angular/core';
import { DataSet, Network } from 'vis/index-network';
import { toast } from 'angular2-materialize';

import { GraphService } from '../app.service';

@Component({
  selector: 'app-friend-graph',
  templateUrl: './friend-graph.component.html',
  providers: [GraphService],
  styleUrls: ['./friend-graph.component.css']
})
export class GraphComponent implements OnInit {
  account: String;
  loading: Boolean;

  constructor(private graphService: GraphService) { 
  	this.account = '';
  	this.loading = false;
  }

  ngOnInit() {
  	
  }

  drawNetwork(){
  	if(this.account!=''){
  		this.loading = true;
	  	this.graphService.getContacts({'account':this.account}).subscribe(response => {
	  		let data_service = this.graphService.getNodesAndEdges(response);

	  		// create an array with nodes
				let nodes = new DataSet(data_service.nodes);

				// create an array with edges
				let edges = new DataSet(data_service.edges);

				// create a network
				let container = document.getElementById('mynetwork');
				let data = {
				  nodes: nodes,
				  edges: edges
				};
				let options = {
					nodes: {
              shape: 'dot',
              size: 16
          },
				  interaction: {
				    navigationButtons: true
				  }
				};
				let network = new Network(container, data, options);
				network.on("stabilizationProgress", function(params) {
          var maxWidth = 496;
          var minWidth = 20;
          var widthFactor = params.iterations/params.total;
          var width = Math.max(minWidth,maxWidth * widthFactor);

          //document.getElementById('bar').style.width = width + 'px';
          //document.getElementById('text').innerHTML = Math.round(widthFactor*100) + '%';
        });
        network.once("stabilizationIterationsDone", function() {
        	this.loading = false;
          /*document.getElementById('text').innerHTML = '100%';
          document.getElementById('bar').style.width = '496px';
          document.getElementById('loadingBar').style.opacity = 0;
          // really clean the dom element
          setTimeout(function () {document.getElementById('loadingBar').style.display = 'none';}, 500);*/
        });
	  	},err => {
        toast("Ocurrió un error en el servidor",5000)
      });

  	}

  }

}
