import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class RouterApi {
    BASE_URL : String;

  constructor() {
    this.BASE_URL = 'http://192.168.18.163:8000/api/';
  }

  
    ApiLogin(){ 
        return this.BASE_URL + 'auth/';
    }

    ApiUser(){
        return this.BASE_URL + 'user/';   
    }

    /* Funciones que retorna las urls de las apis de facebook */
    ApiFacebookGeneral(){
        return this.BASE_URL + 'facebook/general/';
    }


    ApiFacebookFriends(){
        return this.BASE_URL + 'facebook/friends/';
    }


    ApiFacebookPosts(){
        return this.BASE_URL + 'facebook/posts/';
    }


    ApiFacebookphotos(){
        return this.BASE_URL + 'facebook/photos/';
    }

    ApiFacebookReactionsCount(){
        return this.BASE_URL + 'facebook/reactions/';
    }

    /* Funciones que retorna las urls de las apis de twitter */
    ApiTwitter(){
        return this.BASE_URL + 'twitter/';
    }

    ApiTwitterFollowers(){
        return this.BASE_URL + 'twitter/contacts/follower/';
    }

    ApiTwitterFollowing(){
        return this.BASE_URL + 'twitter/contacts/following/';
    }

    ApiTwitterPosts(){
        return this.BASE_URL + 'twitter/posts/';
    }

    /*Funciones que retorna las urls de las apis genericas*/
    ApiSocialScanUser(){
        return this.BASE_URL + 'social_scan/users/';
    }


    ApiSocialScanPosts(){
        return this.BASE_URL + 'social_scan/posts/';
    }

    ApiSocialScanComments(){
        return this.BASE_URL + 'social_scan/comments/';
    }

    ApiSocialScanReactions(){
        return this.BASE_URL + 'social_scan/reactions/';
    }


    ApiSocialScanPhotos(){
        return this.BASE_URL + 'social_scan/photos/';
    }


    ApiSocialScanContacts(){
        return this.BASE_URL + 'social_scan/users-contacts/';
    }


    ApiUserRegister(){
        return this.BASE_URL + 'user/';
    }

    /* Config Urls */
    ApiCredentialTwitter(){
        return this.BASE_URL + 'credential/twitter/';
    }

    ApiCredentialFacebook(){
        return this.BASE_URL + 'credential/facebook/';
    }

    ApiConfigWords(){
        return this.BASE_URL + 'config/words/';
    }

    /* Url Graphics */
    ApiGraphView(){
        return this.BASE_URL + 'social_scan/users-graph/';
    }

    ApiPostByDate(){
        return this.BASE_URL + 'post/date/';
    }

    ApiDangerWords(){
        return this.BASE_URL + 'danger_words/';
    }

    createHeader() {
        let headers = new HttpHeaders({
            'Authorization': 'Token ' + localStorage.getItem('token')
          })
        return headers;
  }
}

@Injectable()
export class GraphService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  getContacts(params){ 
    let url = this.routerApi.ApiGraphView();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header,params: params});
    return response;
  }

  getNodesAndEdges(params){
    let nodes = [];
    let edges = [];
    // Agregado primer nodo
    nodes.push({'id':params[0]['user']['id'],
    'label':params[0]['user']['username'],'group':0})
    let i = 1;
    for(let node of params){
      nodes.push({'id':node['contact']['id'],
      'label':node['contact']['name'],'group':i});
      edges.push({'from':node['contact']['id'],'to':node['user']['id']});
      i++;
    }
    let data = {
      nodes: nodes,
      edges: edges
    };
    return data;
  }

}