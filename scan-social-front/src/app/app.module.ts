import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import 'materialize-css';
import { MaterializeModule } from "angular2-materialize";
import { TagInputModule } from 'ngx-chips';
import { ChartModule } from 'angular-highcharts';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { FacebookComponent } from './facebook/facebook.component';
import { TwitterComponent } from './twitter/twitter.component';
import { RouterApi } from './app.service';
import { ConfigComponent } from './config/config.component';
import { FacebookDbComponent } from './facebook-db/facebook-db.component';
import { TwitterDbComponent } from './twitter-db/twitter-db.component';
import { GraphComponent } from './friend-graph/friend-graph.component';
import { GeneralGraphComponent } from './general-graph/general-graph.component';
import { AccountComponent } from './account/account.component';

const appRoutes : Routes =
  [
    {
      path: '',
      component: AppComponent,
      pathMatch: 'full'
    },
    {
      path: 'home', component: HomeComponent,
      children:[
        { path: 'account', component: AccountComponent, outlet: 'content'},
        { path: 'config', component: ConfigComponent, outlet: 'content'},
        { path: 'facebook/scan', component: FacebookComponent, outlet: 'content'},
        { path: 'facebook/search', component: FacebookDbComponent, outlet: 'content'},
        { path: 'friend/graph', component: GraphComponent, outlet: 'content'},
        { path: 'twitter/scan', component: TwitterComponent, outlet: 'content'},
        { path: 'twitter/search', component: TwitterDbComponent, outlet: 'content'},
        { path: 'graph/general', component: GeneralGraphComponent, outlet: 'content'},
      ]
    },
    {
      path: 'login',
      component: LoginComponent
    }
  ];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    FacebookComponent,
    TwitterComponent,
    ConfigComponent,
    FacebookDbComponent,
    TwitterDbComponent,
    GraphComponent,
    GeneralGraphComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ChartModule,
    HttpClientModule,
    MaterializeModule,
    TagInputModule, 
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [RouterApi],
  bootstrap: [AppComponent]
})
export class AppModule { }
