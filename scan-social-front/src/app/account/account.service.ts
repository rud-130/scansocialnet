import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import "rxjs/add/operator/catch"
import "rxjs/add/observable/throw"

import { RouterApi } from '../app.service';


@Injectable()
export class UserService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }
  
 getUser(): Observable<any[]>{ 
    let url = this.routerApi.ApiUser();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

}