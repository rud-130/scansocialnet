import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { UserService } from './account.service';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers : [UserService]
})
export class AccountComponent implements OnInit {
  username: String;
  mail: String;
  first_name: String;
  last_name: String;
  joined: String;

  constructor(private userService: UserService) {
  	this.username = '';
  	this.mail = '';
  	this.first_name = '';
  	this.last_name = '';
  	this.joined = '';
  }

  ngOnInit() {
  	this.loadUser();
  }

  loadUser(){
  	this.userService.getUser().subscribe(response => {
      if(JSON.stringify(response)!='{}'){
      	this.username = response['username'];
      	this.mail = response['email'];
      	this.first_name = response['first_name'];
      	this.last_name = response['last_name'];
      	let pipe = new DatePipe('en-US')
      	this.joined = pipe.transform(response['date_joined'],'dd/mm/yyyy hh:mm:ss a');
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

}
