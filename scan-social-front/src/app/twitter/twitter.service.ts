import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RouterApi } from '../app.service';


@Injectable()
export class TwitterService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 getBasic(params){ 
    let url = this.routerApi.ApiTwitter();    
    let header = this.routerApi.createHeader();
    let response = this.http.post(url,params,{headers: header});
    return response;
  }

  getFollowers(params){ 
    let url = this.routerApi.ApiTwitterFollowers();    
    let header = this.routerApi.createHeader();
    let response = this.http.post(url,params,{headers: header});
    return response;
  }

  getFollowing(params){ 
    let url = this.routerApi.ApiTwitterFollowing();    
    let header = this.routerApi.createHeader();
    let response = this.http.post(url,params,{headers: header});
    return response;
  }

  getPosts(params){ 
    let url = this.routerApi.ApiTwitterPosts();    
    let header = this.routerApi.createHeader();
    let response = this.http.post(url,params,{headers: header});
    return response;
  }

}