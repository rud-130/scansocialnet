import { Component, OnInit } from '@angular/core';
import { toast } from 'angular2-materialize';
import { TwitterService } from './twitter.service';

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  providers: [TwitterService],
  styleUrls: ['./twitter.component.css']
})
export class TwitterComponent implements OnInit {
    account: String;
    msj: any;
    user_info: any;
    friends_info: any;
    follow_info: any;
    posts_info: any;
    loading: Boolean;
    follow_loading: Boolean;
    following_loading: Boolean;
    posts_loading: Boolean;
    count: number;
    constructor(private twitterService: TwitterService) {
        this.loading = false;
        this.follow_loading = false;
        this.following_loading = false;
        this.count = 0;
        this.user_info = false;
        this.msj = false;
    }

    ngOnInit() {
    }
    search() {
        this.loading = true;
        this.twitterService.getBasic({'account':this.account}).subscribe(response => {
            if(response['success']==true){
                this.user_info = response['data_user'];
                this.msj = false;
                this.loading = false;
            }
            else{
                this.user_info = false;
                this.msj = response['mensaje'];
                toast(response['mensaje'],5000)
                this.loading = false;
            }      
        },err => {
          toast("Ocurrió un error en el servidor",5000)
        });
    }

    searchFollower() {
        this.follow_loading = true;
        this.twitterService.getFollowers({'account':this.account}).subscribe(response => {
            if(response['success']==true){
                this.follow_info = response;
                this.follow_loading = false;
            }
            else{
                this.follow_info = response;
                this.follow_loading = false;
            }
        },err => {
          toast("Ocurrió un error en el servidor",5000)
        });
    }

    searchFriends() {
        this.following_loading = true;
        this.twitterService.getFollowing({'account':this.account}).subscribe(response => {
            if(response['success']==true){
                this.friends_info = response;
                this.following_loading = false;
            }
            else{
                this.friends_info = response;
                this.following_loading = false;
            }        
  
        },err => {
          toast("Ocurrió un error en el servidor",5000)
        });
    }

    searchPosts(){
        if(this.count!=0){
            this.posts_loading = true;
            this.twitterService.getPosts({'account':this.account, 'count':this.count}).subscribe(response => {
                if(response['success']==true){
                    this.posts_info = response;
                    this.posts_loading = false;
                }
                else{
                    this.posts_info = response;
                    this.posts_loading = false;
                }
            },err => {
              toast("Ocurrió un error en el servidor",5000)
            });
        }
        else{
            console.log("Debe ingresar el numero de posts a rastrear");
        }
      
    }

}
