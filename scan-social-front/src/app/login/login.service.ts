import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import "rxjs/add/operator/catch"
import "rxjs/add/observable/throw"

import { RouterApi } from '../app.service';


@Injectable()
export class LoginService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 login(params): Observable<any[]>{ 
    let url = this.routerApi.ApiLogin();
    let response = this.http.post(url,params)
    .catch((err) => {
    	return Observable.throw(err)
    });
    return response;
  }


  logout(){
    localStorage.removeItem('token');
  }


}