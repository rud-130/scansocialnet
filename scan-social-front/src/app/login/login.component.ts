import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { toast } from 'angular2-materialize';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [LoginService],
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	username: String;
	password: String;
	
  constructor(private router: Router, private loginService: LoginService) { 
    this.username='';
    this.password='';
  }

  ngOnInit() {
  }

  login(){
  	if(this.username!='' && this.password!=''){
	  	let rest =  this.loginService.login({'username':this.username,
	  	'password':this.password}).subscribe(response => {
          if(response['token']!=''){
            localStorage.setItem('token',response['token']);
            this.router.navigate(['home']);
          }
      },err => {
        if (err.status == 400 ){
          toast(err['error']['non_field_errors'][0], 5000);
        }
        else{
          toast("Ocurrió un error en el servidor", 5000);  
        }
      });
  	}
  	else{
  		toast("Debe ingresar todos los parametros", 5000);
  	}
  }

}

