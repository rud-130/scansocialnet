import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { RouterApi } from '../app.service';


@Injectable()
export class FacebookDBService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  getUser(params){ 
    let url = this.routerApi.ApiSocialScanUser();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header,params: params});
    return response;
  }

  getFriends(params){ 
    let url = this.routerApi.ApiSocialScanContacts();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header,params: params});
    return response;
  }

  getPhotos(params){ 
    let url = this.routerApi.ApiSocialScanPhotos();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header,params: params});
    return response;
  }

  getPosts(params){ 
    let url = this.routerApi.ApiSocialScanPosts();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header,params: params});
    return response;
  }

  getPostComments(params){ 
    let url = this.routerApi.ApiSocialScanComments();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header,params: params});
    return response;
  }

  getPostReactions(params){ 
    let url = this.routerApi.ApiSocialScanReactions();
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header,params: params});
    return response;
  }

  getMoreData(url){ 
    let header = this.routerApi.createHeader();
    
    let response = this.http.get(url,{headers: header});
    return response;
  }


}