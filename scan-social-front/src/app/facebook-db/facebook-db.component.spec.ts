import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacebookDbComponent } from './facebook-db.component';

describe('FacebookDbComponent', () => {
  let component: FacebookDbComponent;
  let fixture: ComponentFixture<FacebookDbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacebookDbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookDbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
