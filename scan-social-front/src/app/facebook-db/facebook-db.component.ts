import { Component, OnInit, ElementRef } from '@angular/core';
import { FacebookDBService } from './facebook-db.service';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-facebook-db',
  templateUrl: './facebook-db.component.html',
  providers: [FacebookDBService],
  styleUrls: ['./facebook-db.component.css']
})
export class FacebookDbComponent implements OnInit {
  account: String;
  photo_url: String;
  friend_url: String;
  post_url: String;
  loading: Boolean;
  photo_more: Boolean;
  post_more: Boolean;
  friend_more: Boolean;
  user: any;
  photos: any;
  friends: any;
  posts: any;

  constructor(private facebookDbService: FacebookDBService, private elementRef:ElementRef) { 
  	this.account = '';
    this.friend_url = '';
    this.photo_url = '';
    this.post_url = '';
    this.loading = false;
    this.user = false;
    this.photo_more = false;
    this.post_more = false;
    this.friend_more = false;
  }

  ngOnInit() {
  }

  searchUser(){
    let object = this;
  	if(this.account!=''){
      this.loading = true;
  	  this.facebookDbService.getUser({'id_user':this.account}).subscribe(response => {
        if(response['results'].length!=0){
          this.user = response['results'][0];
          this.searchPhotos();
          this.searchFriends();
          this.searchPosts();
          this.loading = false;
        }	
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
  	}
  }

  searchPhotos(){
    this.photo_more = false;
    this.facebookDbService.getPhotos({'id_user':this.account}).subscribe(response => {
        if(response['results'].length!=0){
          this.photos = response['results'];
        }
        if(response['next']){
          this.photo_url = response['next'];
          this.photo_more = true;
        }
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
  }

  searchFriends(){
    this.friend_more = false;
    this.facebookDbService.getFriends({'user':this.account}).subscribe(response => {
        if(response['results'].length!=0){
          this.friends = response['results'];
        }
        if(response['next']){
          this.friend_url = response['next'];
          this.friend_more = true;
        }
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
  }

  searchPosts(){
    this.post_more = false;
    this.facebookDbService.getPosts({'id_user':this.account}).subscribe(response => {
      if(response['results'].length!=0){
        this.posts = response['results'];
      }
      if(response['next']){
        this.post_url = response['next'];
        this.post_more = true;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  more_photos(){
    this.photo_more = false;
    let html_content = this.elementRef.nativeElement.querySelector('.more_photo');
    this.facebookDbService.getMoreData(this.photo_url).subscribe(response => {
      if(response['results'].length!=0){
        let html = '';
        for(let photo of response['results']){
          html += '<div class="row">';
          html += '<img src="'+photo.url+'" class="responsive-img"></div>';
        }
        html_content.insertAdjacentHTML('beforeend', html);
      }
      if(response['next']){
        this.photo_url = response['next'];
        this.photo_more = true;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  more_friends(){
    this.friend_more = false;
    let html_content = this.elementRef.nativeElement.querySelector('.more_friends');
    this.facebookDbService.getMoreData(this.friend_url).subscribe(response => {
      if(response['results'].length!=0){
        let html = '';
        for(let friend of response['results']){
          html += '<li class="collection-item">';
          html += friend.contact.name+'</li>';
        }
        html_content.insertAdjacentHTML('beforeend', html);
      }
      if(response['next']){
        this.friend_url = response['next'];
        this.friend_more = true;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  load_posts(){
    this.post_more = false;
    let html_content = this.elementRef.nativeElement.querySelector('.more_posts');
    this.facebookDbService.getMoreData(this.post_url).subscribe(response => {
      if(response['results'].length!=0){
        let html = '';
        for(let posts of response['results']){
          html += '<li class="collection-item">';
          html += posts.historia;
          html += '<b>Reacciones: </b>'+posts.reaction+'<br/>';
          html += '<small>'+posts.fecha_creacion+'</small></li>';
        }
        html_content.insertAdjacentHTML('beforeend', html);
      }
      if(response['next']){
        this.post_url = response['next'];
        this.post_more = true;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

}

