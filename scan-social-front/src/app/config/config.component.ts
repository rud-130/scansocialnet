import { Component, OnInit } from '@angular/core';
import { ConfigService } from './config.service';
import { TagInputModule } from 'ngx-chips';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  providers: [ConfigService],
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {
  facebook_user: String;
  facebook_password: String;
  twitter_consumer_key: String;
  twitter_consumer_secret: String;
  twitter_access_token: String;
  twitter_access_token_secret: String;
  danger_words: any;
  window: any;

  constructor(private configService: ConfigService) { 
    this.facebook_user = '';
    this.facebook_password = '';
    this.twitter_consumer_key = '';
    this.twitter_consumer_secret = '';
    this.twitter_access_token = '';
    this.twitter_access_token_secret = '';
    this.danger_words = [];
  }

  ngOnInit() {
    this.configService.getFacebook().subscribe(response => {
      if(response.length>0){
        this.facebook_user = response[0].email_phone;
        this.facebook_password = response[0].password;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
    this.configService.getTwitter().subscribe(response => {
      if(response.length>0){
        this.twitter_consumer_key = response[0].consumer_key;
        this.twitter_consumer_secret = response[0].consumer_secret;
        this.twitter_access_token = response[0].access_token;
        this.twitter_access_token_secret = response[0].access_token_secret;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
    this.configService.getDangerWords().subscribe(response => {
      if(response.length>0){
        this.danger_words = response[0].word.split(";");
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  register_facebook(){
    if(this.facebook_user!='' && this.facebook_password!=''){
      let params = {
        'email_phone':this.facebook_user,
        'password':this.facebook_password
      }
      this.configService.registerFacebook(params).subscribe(response => {
        if (response['error']){
         toast("Existe un error al guardar las credenciales de facebook" + response['error']['email_phone'], 3000)
        }
        else{
          toast("Exito al guardar las credenciales de facebook", 5000)
        }
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });

    }

  }

  register_twitter(){
    if(this.twitter_consumer_key!='' && this.twitter_consumer_secret!=''
    && this.twitter_access_token!='' && this.twitter_access_token_secret!=''){
      let params = {
        'consumer_key':this.twitter_consumer_key, 'consumer_secret':this.twitter_consumer_secret,
        'access_token':this.twitter_access_token, 'access_token_secret':this.twitter_access_token_secret
      }
      this.configService.registerTwitter(params).subscribe(response => {
        console.log(response);
        toast("Exito al guardar las credenciales de twitter", 5000)
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
    
    }

  }

  register_dangerwords(){
    if(this.danger_words.length>0){
      let join = '';
      for(let item of this.danger_words){
        join += item.display+";";
      }
      join = join.substring(0,join.length-1);
      let params = {
        'word':join
      }
      this.configService.registerDangerWords(params).subscribe(response => {
        console.log(response);
        toast("Exito al guardar las palabras", 5000)
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
    }
  }

}

