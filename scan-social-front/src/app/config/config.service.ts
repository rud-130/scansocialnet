import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import "rxjs/add/operator/catch"
import "rxjs/add/observable/throw"

import { RouterApi } from '../app.service';


@Injectable()
export class ConfigService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 registerFacebook(params){ 
    let url = this.routerApi.ApiCredentialFacebook();
    let header = this.routerApi.createHeader();
    let response = this.http.post(url,params,{headers:header})
    .catch((err) => {
    	return Observable.throw(err)
    });
    return response;
  }

  getFacebook(): Observable<any[]>{ 
    let url = this.routerApi.ApiCredentialFacebook();
    let header = this.routerApi.createHeader();
    let response = this.http.get(url,{headers:header})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

  registerTwitter(params){ 
    let url = this.routerApi.ApiCredentialTwitter();
    let header = this.routerApi.createHeader();
    let response = this.http.post(url,params,{headers:header})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

  getTwitter(): Observable<any[]>{ 
    let url = this.routerApi.ApiCredentialTwitter();
    let header = this.routerApi.createHeader();
    let response = this.http.get(url,{headers:header})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

  getDangerWords(): Observable<any[]>{ 
    let url = this.routerApi.ApiConfigWords();
    let header = this.routerApi.createHeader();
    let response = this.http.get(url,{headers:header})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

  registerDangerWords(params){
    let url = this.routerApi.ApiConfigWords();
    let header = this.routerApi.createHeader();
    let response = this.http.post(url,params,{headers:header})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

}