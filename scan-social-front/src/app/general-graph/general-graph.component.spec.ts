import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralGraphComponent } from './general-graph.component';

describe('GeneralGraphComponent', () => {
  let component: GeneralGraphComponent;
  let fixture: ComponentFixture<GeneralGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
