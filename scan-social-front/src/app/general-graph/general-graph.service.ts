import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import "rxjs/add/operator/catch"
import "rxjs/add/observable/throw"

import { RouterApi } from '../app.service';


@Injectable()
export class GeneralGraphService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  getUserInteraction(params): Observable<any[]>{ 
    let url = this.routerApi.ApiFacebookReactionsCount();
    let header = this.routerApi.createHeader();
    let response = this.http.get(url,{headers:header,params: params})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

  getPostByDate(params): Observable<any[]>{ 
    let url = this.routerApi.ApiPostByDate();
    let header = this.routerApi.createHeader();
    let response = this.http.get(url,{headers:header,params: params})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

  getDangerWords(params): Observable<any[]>{ 
    let url = this.routerApi.ApiDangerWords();
    let header = this.routerApi.createHeader();
    let response = this.http.get(url,{headers:header,params: params})
    .catch((err) => {
      return Observable.throw(err)
    });
    return response;
  }

}