import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { toast } from 'angular2-materialize';
import { GeneralGraphService } from './general-graph.service';


@Component({
  selector: 'app-general-graph',
  templateUrl: './general-graph.component.html',
  styleUrls: ['./general-graph.component.css'],
  providers: [GeneralGraphService]
})
export class GeneralGraphComponent implements OnInit {
  loading_friends: Boolean;
  loading_post: Boolean;
  loading_danger: Boolean;
  search_it: Boolean;
  account: String;
  init_date: String;
  end_date: String;
  user_reactions: any;
  dias_semana: any;
  series: any;
  object_series: any;
  danger: any;
  post_semana_anio: String;
  danger_post_color: String;
  danger_photo_color: String;
  danger_post_percent: Number;
  danger_photo_percent: Number;
  select_date: any;
  chart: Chart;

  constructor(private generalGraphService: GeneralGraphService) { 
  	this.loading_friends = false;
    this.loading_post = false;
    this.loading_danger = false;
    this.search_it = false;
    this.account = '';
    this.init_date = '';
    this.end_date = '';
    this.user_reactions = [];
    this.dias_semana = {"Lun": 0, "Mar": 1, "Mié": 2, "Jue": 3, "Vie": 4, "Sáb": 5, "Dom": 6}
    this.series = [0, 0, 0, 0, 0, 0, 0];
    this.danger = {};
    this.object_series = {};
    this.select_date = [];
    this.danger_post_color = '';
    this.post_semana_anio = '';
    this.danger_photo_color = '';
    this.danger_post_percent = 0;
    this.danger_photo_percent = 0;
  }

  ngOnInit() {
    this.init();
  }

  drawnGraphics(){
    this.obtainPostReactions();
    this.obtainDanger();
    this.search_it = true;
  }

  obtainPostReactions(){
    if(this.account!=''){
      this.loading_friends = true;
      this.generalGraphService.getUserInteraction({'account':this.account}).subscribe(response => {
        if(response['error']){
          console.log(response['error']);
        }
        else if(response['data'].length>0){
          let disorded_list = []
          for(let react of response['data']){
            let total = (react.reacciones/response['total'])*100;
            disorded_list.push({'name':react.autor,'percent':total.toFixed(3)})
          }
          let orderNum = Object.keys(disorded_list).sort(
            function(a,b){
              return disorded_list[b]['percent']-disorded_list[a]['percent'];
            }
          );
          let i=0;
          for(let num of orderNum){
            console.log(num);
            this.user_reactions[i] =  disorded_list[num];
            i += 1;
          }
        }
        this.loading_friends = false;
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
    }
  }

  obtainPostDate(){
    if(this.account!='' && this.init_date!='' && this.end_date!=''){
          this.loading_post = true;
          this.generalGraphService.getPostByDate({'account': this.account, 'init_date': this.init_date, 'end_date': this.end_date})
          .subscribe(response => {
              if(JSON.stringify(response) != '{}'){
                let sem_anio = Object.keys(response)
                this.object_series = response;
                this.select_date = sem_anio.sort();
                this.post_semana_anio = this.select_date[0];
                for(let datos of response[this.select_date[0]]){
                  if(Object.keys(this.dias_semana).includes(datos['dia_semana'])){
                    console.log(datos['dia_semana'], this.dias_semana[datos['dia_semana']]);
                    this.series.splice(this.dias_semana[datos['dia_semana']], 1);
                    this.series.splice(this.dias_semana[datos['dia_semana']], 0, datos['num_posts']);
                    console.log(this.series)
                    this.series = this.series.slice(0, 7);
                  }
                }
                this.chart.removeSerie(this.chart.ref.series.length - 1);
                this.chart.addSerie({
                      name: 'Interacciones del Usuario',
                      data: this.series
                });
              }
              else{
                toast("No se encuentran datos para este rango de fecha",5000);
              }
              this.loading_post = false; 
            },err => {
              toast("Ocurrió un error en el servidor",5000)
            });
      }
      else{
        console.log("Necesita todos los parametros");
      }
  }

 
  init() {
    let chart = new Chart({
      chart: {
        type: 'spline'
      },
      title: {
        text: 'Grafica de Actividad'
      },
      credits: {
        enabled: false
      },
      xAxis:
      {
        categories: ['Lun', 'Mar', 'Mir', 'Jue', 'Vie', 'Sab', 'Dom']
      },
      yAxis: {
        title: {
            text: 'Posts'
        },
      },
      plotOptions: {
        spline: {
          lineWidth: 4,
          states: {
            hover: {
              lineWidth: 5
            }
          },
          marker: {
            enabled: false
          },
        },
        series: {
            color: '#7cb'
          }
      },
      series: [{
        name: 'Interacciones del Usuario',
        data: this.series
      }]
    });

    //chart.addPoint(4);
    this.chart = chart;

    /*chart.addPoint(5);
    setTimeout(() => {
      chart.addPoint(6);
    }, 2000);*/
  }

  obtainDanger(){
    if(this.account!=''){
      this.loading_danger = true;
      this.generalGraphService.getDangerWords({'account':this.account}).subscribe(response => {
        if(response){
          this.danger = response;
          if(this.danger['post_data'].length > 0){
            this.danger_post_percent = this.danger['post_data'][0]['danger_percent'].toFixed(3);
            this.danger_post_color = this.getColor(this.danger_post_percent);
          }
          if(this.danger['photo_data'].length > 0){
            this.danger_photo_percent = this.danger['photo_data'][0]['danger_percent'].toFixed(3);
            this.danger_photo_color = this.getColor(this.danger_photo_percent);
          }
          this.loading_danger = false;
        }
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
    }
  }

  getColor(percent){
    if(percent<=25){
      return 'green';
    }
    else if(percent>25 && percent<50){
      return 'yellow';
    }
    else if(percent>50 && percent<75){
      return 'deep-orange';
    }
    else if(percent>75){
      return 'red accent-4';
    }
    return '';
  }

  setSemanaAnio(sema_anio, object_series){
    let sem_anio = Object.keys(object_series)
      this.object_series = object_series;
      this.select_date = sem_anio.sort();
      this.post_semana_anio = this.select_date[sema_anio];
      for(let datos of object_series[this.select_date[sema_anio]]){
        if(Object.keys(this.dias_semana).includes(datos['dia_semana'])){
          console.log(datos['dia_semana'], this.dias_semana[datos['dia_semana']]);
          this.series.splice(this.dias_semana[datos['dia_semana']], 1);
          this.series.splice(this.dias_semana[datos['dia_semana']], 0, datos['num_posts']);
          console.log(this.series)
          this.series = this.series.slice(0, 7);
        }
      }
      this.chart.removeSerie(this.chart.ref.series.length - 1);
      this.chart.addSerie({
            name: 'Interacciones del Usuario',
            data: this.series
      });
    }

}

