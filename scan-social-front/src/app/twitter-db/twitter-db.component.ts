import { Component, OnInit, ElementRef } from '@angular/core';
import { toast } from 'angular2-materialize';
import { FacebookDBService } from '../facebook-db/facebook-db.service';

@Component({
  selector: 'app-twitter-db',
  templateUrl: './twitter-db.component.html',
  providers: [FacebookDBService],
  styleUrls: ['./twitter-db.component.css']
})
export class TwitterDbComponent implements OnInit {
  account: String;
  post_url: String;
  friend_url: String;
  loading: boolean;
  post_more: boolean;
  friend_more: boolean;
  user: any;
  posts:any;
  friends: any;
  constructor(private twitterDbService: FacebookDBService, private elementRef:ElementRef) { 
      this.account = '';
      this.post_url = '';
      this.friend_url = '';
      this.loading = false;
      this.post_more = false;
      this.friend_more = false;
  }

  ngOnInit() {
  }

  searchUser(){
      if(this.account!=''){
          this.loading = true;
            this.twitterDbService.getUser({'username':this.account}).subscribe(response => {
            if(response['results'].length!=0){
              this.user = response['results'][0];
              this.searchPosts();
              this.searchFriends()
              this.loading = false;
            }    
        },err => {
          toast("Ocurrió un error en el servidor",5000)
        });
      }
  }

  searchPosts(){
    this.post_more = false;
    this.twitterDbService.getPosts({'username':this.account}).subscribe(response => {
      
      if(response['results'].length!=0){
        this.posts = response['results'];
        console.log(this.posts)
      }
      if(response['next']){
        this.post_url = response['next'];
        this.post_more = true;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

  more_posts(){
    this.post_more = false;
    let html_content = this.elementRef.nativeElement.querySelector('.more_post');
    this.twitterDbService.getMoreData(this.post_url).subscribe(response => {
      if(response['results'].length!=0){
        let html = '';
        for(let post of response['results']){
          html += '<li class="collection-item">';
          html += post.user.username;
          html += '<div class="card horizontal">';
          if (post.img){
              html += '<div class="card-image">';
              html += '<img src="'+post.img+'">';
              html += '</div>';
          }
          html += '<div class="card-stacked">';
          html += '<div class="card-content">'
          html += '<h5> Tipo de post: </h5>"'+post.historia+'"'
          html += '<p>"'+post.mensaje+'"</p>'
          html += '</div>'
          html += '<div class="card-action">'
          html += '<i class="material-icons">thumb_up</i> '+ post.reaction
          html += '<i class="material-icons">screen_share</i> '+ post.reaction
          html += '</div>'
          html += '</div>'
          html += '</div>'
          html += '</li>';
             
        }
        html_content.insertAdjacentHTML('beforeend', html);
      }
      if(response['next']){
        this.post_url = response['next'];
        this.post_more = true;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });

  }

  searchFriends(){
    this.friend_more = false;
    this.twitterDbService.getFriends({'user':this.account}).subscribe(response => {
        if(response['results'].length!=0){
          this.friends = response['results'];
        }
        if(response['next']){
          this.friend_url = response['next'];
          this.friend_more = true;
        }
      },err => {
        toast("Ocurrió un error en el servidor",5000)
      });
  }

  more_friends(){
    this.friend_more = false;
    let html_content = this.elementRef.nativeElement.querySelector('.more_friends');
    this.twitterDbService.getMoreData(this.friend_url).subscribe(response => {
      if(response['results'].length!=0){
        let html = '';
        for(let friend of response['results']){
          html += '<li class="collection-item">';
          
          if (friend.contact.tipo == 'Followers'){
                html += '<div class="card-panel teal">';
                html += '<p>Follow</p>';
                html += '<span class="white-text">'+friend.contact.name;
                html += '</span>';
                html += '</div>';

          }
          else if (friend.contact.tipo == 'Followings') { 
                html += '<div class="card-panel blue lighten-1">';
                html += '<p>Following</p>';
                html += '<span class="white-text">'+friend.contact.name;
                html += '</span>';
                html += '</div>';
          } 
          else {
             html += '<p>Lo sentimos este amigo no esta disponible </p>';
          }
          html += '</li>';
        
        }
        html_content.insertAdjacentHTML('beforeend', html);
      }
      if(response['next']){
        this.friend_url = response['next'];
        this.friend_more = true;
      }
    },err => {
      toast("Ocurrió un error en el servidor",5000)
    });
  }

}
