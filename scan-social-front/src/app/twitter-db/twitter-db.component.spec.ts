import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwitterDbComponent } from './twitter-db.component';

describe('TwitterDbComponent', () => {
  let component: TwitterDbComponent;
  let fixture: ComponentFixture<TwitterDbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwitterDbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwitterDbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
