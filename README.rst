Para instalar la apliacacion en modo desarrollo debera seguir los siguientes pasos:

1-) Instalar el controlador de versiones git:
    
    $ su

    # aptitude install git

2-) Descargar el codigo fuente del proyecto scansocialnet:

    Para descargar el código fuente del proyecto contenido en su repositorio GIT realice un clon del proyecto scansocialnet, clonar con el siguiente comando:

    $ git clone https://gitlab.com/rud-130/scansocialnet.git

3-) Crear un Ambiente Virtual:

    El proyecto está desarrollado con el lenguaje de programación Python, se debe instalar Python v3.4.2. Con los siguientes comandos puede instalar Python y PIP.

    Entrar como root para la instalacion 

    # aptitude install python3.4 python3-pip python3.4-dev python3-setuptools

    # aptitude install python3-virtualenv virtualenvwrapper

    Salir del modo root y crear el ambiente:

    $ mkvirtualenv --python=/usr/bin/python3 scansocialnet

4-) Instalar mongoDB:
    
    seguir las instrucciones de la documentacion oficial del siguiente enlace:

    https://docs.mongodb.com/manual/administration/install-on-linux/

5-) Instalar los requerimientos del proyecto 

    Para activar el ambiente virtual scansocialnet ejecute el siguiente comando:

    $ workon scansocialnet

    (scansocialnet)$

    Entrar en la carpeta raiz del proyecto:

    (scansocialnet)$ cd scansocialnet
    
    (scansocialnet)scansocialnet$ 

    Desde ahi se deben instalar los requirimientos del proyecto con el siguiente comando:

    (scansocialnet)$ pip install -r requeriments.txt

    Instalar stopwords:

    Se debe usar el siguiente comando para su instalación:
    python -m nltk.downloader stopwords

6-) Antes de iniciar la aplicación se deben modificar los siguientes archivos para la comunicación entre el backend  y frontend:

    Configurar CORS en el Backend:
    Ir a la raíz del proyecto y ubicar el archivo settings.py el cual se encuentra en la siguiente ruta: scansocialnet/scansocialnet/settings.py
    
    Se debe editar las siguiente constante CORS_ORIGIN_WHITELIST en el settings.py de la aplicación y agregar la dirección ip donde estará alojado el frontend:
    
    Ejemplo:

    CORS_ORIGIN_WHITELIST = (
        '192.168.18.163:4200',
        '192.168.137.101:4200',
        'IPFrontend:4200,
        )

    Esta constante puede contener tantas ips como el administrador de la plataforma lo considere.

    Configurar inicio y conexión desde el Frontend a las apis del backend:
    
    Para configurar el inicio o ejecución del frontend se debe ubicar el archivo package.json, el cual se encuentra ubicado en la siguiente ruta:

    scansocialnet/scan-social-front/package.json: desde ahí se debe editar la regla “start” y colocar la dirección ip desde donde será ejecutado el servicio.

    Ejemplo:
    
    "start": "ng serve --host 192.168.137.101",

    Para sincronizar las apis del backend con el frontend se debe ubicar el archivo app.service.ts, el cual se encuentra ubicado en la siguiente ruta: scansocialnet/scan-social-front/src/app/app.service.ts: desde ahí se debe edita la constante BASE_URL y se le debe asignar la ruta donde está ejecutando el backend.
    
    Ejemplo:

    this.BASE_URL = 'http://IPBackend:8000/api/';


7-) Iniciar el backend de la aplicación scansocialnet;

    Crear un super usuario:

    (scansocialnet)$ python manage.py createsuperuser 

    Para iniciar el backend de la aplicación se debe  ejecutar el siguiente comando:
    (scansocialnet)$ python manage.py runserver

    Ingresar a la siguiente url:
    localhost:8000/api  o la dirección ip por la que inicio el proyecto Django o Backend.

8-) Instalar nodejs:
    
    Seguir las instrucciones de la documentación oficial del siguiente enlace se encuentras las distros para linux:

    https://nodejs.org/es/download/package-manager/

    Una vez instalado puede comprobar que la instalación fue exitosa con los siguientes comandos:
    $node -v
    Este comando muestra la versión actual instalada y 
    $npm –v
    Muestra la versión del manejador de paquetería de nodejs que está instalada.

    Una vez culminado este proceso de instalación ir al siguiente paso.


9-) Iniciar el frontend de la aplicación:

    Para iniciar el frontend de la aplicación se debe ingresar a la carpeta raíz del directorio del proyecto que se encarga del frontend el cual se encuentra ubicado en la siguiente ruta: scansocialnet/scan-social-front/

    Desde esa ubicación ejecutar el siguiente comando:

    $ npm install

    Esto ejecuta la instalación de los módulos de node en el proyecto, los cuales son necesarios para el correcto funcionamiento, una vez termine la instalación de los paquetes para el proyecto, se ejecuta el siguiente comando:

    $ npm start

    Esto inicia el servidor para el frontend realizando una comprobación de las paqueterías y las fuentes que usa el modulo en el frontend una vez se compile con éxito el proyecto puede ingresar al navegador de su preferencia en la ruta especificada para iniciar el frontend la cual fue editada en el package.json.

    Ejemplo:
        http://192.168.137.101:4200/

