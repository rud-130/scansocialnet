from rest_framework import views, mixins, permissions, exceptions, parsers, renderers
from rest_framework.response import Response
from rest_framework_mongoengine import viewsets
from rest_framework import viewsets as viewset

from users.serializers import *
from users.models import *
from users.authentication import TokenAuthentication

from django.views.generic import (
    FormView, RedirectView, CreateView, 
    UpdateView, ListView, TemplateView
    )
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.hashers import check_password
from django.core.urlresolvers import reverse_lazy
from django.core import serializers

from .forms import LoginForm


class UserViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    """
    Read-only User endpoint
    """
    permission_classes = (permissions.IsAuthenticated, )  # IsAdminUser?
    authentication_classes = (TokenAuthentication, )
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all()

    def list(self, request, format=None):
        try:
            queryset = User.objects.get(username=request.user.username)
            serializer = UserSerializer(queryset)
        except Exception as e:
            return Response([e])
        return Response(serializer.data)


class ObtainAuthToken(views.APIView):
    throttle_classes = ()
    permission_classes = ()
    authentication_classes = (TokenAuthentication, )
    # parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    # renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        return Response({'token': token.key})


obtain_auth_token = ObtainAuthToken.as_view()


class RegisterUser(viewset.ViewSet):
    """
        Class costumizada para registrar un usuario en la plataforma

        # @author Leonel Hernández (leonelphm at gmail)
        # @author Rodrigo Boet (rudmanmrrod at gmail)
        # @Date 06/03/2018
        # @version 1.0.0
        > Parameters:

          * Create: POST register/user/ => **username, password, email**.
    """
    serializer_class = RegisterUserSerializer

    def create(self, request, format=None):
        """
            Método para el registro de usuarios
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        serialized = self.serializer_class(data=request.data)

        if serialized.is_valid():
            User.create_user(
                serialized.data['username'],
                serialized.data['password'],
                serialized.data['email'],
            )
            return Response(serialized.data)
        else:
            return Response(serialized._errors)


class LoginView(FormView):
    """!
    Clase que gestiona la vista principal del logeo de usuario

    @date 29-04-2018
    @version 1.0.0
    """
    form_class = LoginForm
    template_name = 'user.login.html'
    success_url = reverse_lazy('inicio')

    def form_valid(self, form):
        """!
        Metodo que valida si el formulario es valido
    
        @param self <b>{object}</b> Objeto que instancia la clase
        @param form <b>{object}</b> Objeto que contiene el formulario de registro
        @return Retorna el formulario validado
        """
        usuario = form.cleaned_data['usuario']
        contrasena = form.cleaned_data['contrasena']
        usuario = authenticate(username=usuario, password=contrasena)
        login(self.request, usuario)
        if self.request.POST.get('remember_me') is not None:
            # Session expira a los dos meses si no se deslogea
            self.request.session.set_expiry(1209600)
        return super(LoginView, self).form_valid(form)
    
    
class LogoutView(RedirectView):
    """!
    Clase que gestiona la vista principal del deslogeo de usuario

    @date 29-04-2018
    @version 1.0.0
    """
    permanent = False
    query_string = True

    def get_redirect_url(self):
        """!
        Metodo que permite definir la url de dirección al ser válido el formulario
    
        @date 01-03-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Retorna la url
        """
        logout(self.request)
        return reverse_lazy('login')
