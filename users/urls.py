"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package users.url
#
# Urls de la aplicación
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^auth/$', ObtainAuthToken.as_view(), name='auth'),
]
