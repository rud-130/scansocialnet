"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package users.forms
#
# Formulario correspondiente a la aplicación users
# @version 1.0
from django import forms
from django.forms import ModelForm
from django.contrib.auth import authenticate
from django.contrib.auth.forms import (
    UserCreationForm
)
from django.forms.fields import (
    CharField, BooleanField
)
from django.forms.widgets import (
    PasswordInput, CheckboxInput
)
from django_mongoengine.forms import DocumentForm

from .models import User

def validate_email(email):
    """!
    Función que permite validar la cedula

    @date 20-04-2017
    @param cedula {str} Recibe el número de cédula
    @return Devuelve verdadero o falso
    """
    
    email = User.objects.filter(email=email)
    if email:
        return True
    else:
        return False

def validate_username(username):
    """!
    Función que permite validar el nombre de usuario

    @date 20-09-2017
    @param username {str} Recibe el nombre de usuario
    @return Devuelve verdadero o falso
    """
    
    usr = User.objects.filter(username=username)
    if usr:
        return True
    else:
        return False

class LoginForm(forms.Form):
    """!
    Clase del formulario de logeo

    @date 01-03-2018
    @version 1.0.0
    """
    ## Campo de la constraseña
    contrasena = CharField()

    ## Nombre del usuario
    usuario = CharField()

    ## Formulario de recordarme
    remember_me = BooleanField()

    ## Campo del captcha
    #captcha = CaptchaField()

    def __init__(self, *args, **kwargs):
        """!
        Metodo que sobreescribe cuando se inicializa el formulario

        @date 01-03-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @param args <b>{list}</b> Lista de los argumentos
        @param kwargs <b>{dict}</b> Diccionario con argumentos
        @return Retorna el formulario validado
        """
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['contrasena'].widget = PasswordInput()
        self.fields['contrasena'].widget.attrs.update({'class': 'validate',
        'placeholder': 'Contraseña'})
        self.fields['usuario'].widget.attrs.update({'class': 'validate',
        'placeholder': 'Nombre de Usuario'})
        self.fields['remember_me'].label = "Recordar"
        self.fields['remember_me'].widget = CheckboxInput()
        self.fields['remember_me'].required = False
        #self.fields['captcha'].label = "Captcha"
        #self.fields['captcha'].widget.attrs.update({'class': 'validate'})

    def clean(self):
        """!
        Método que valida si el usuario a autenticar es valido

        @date 21-04-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Retorna el campo con los errores
        """
        usuario = self.cleaned_data['usuario']
        contrasena = self.cleaned_data['contrasena']
        usuario = authenticate(username=usuario,password=contrasena)
        if(not usuario):
            msg = "Verifique su usuario o contraseña"
            self.add_error('usuario', msg)

    class Meta:
        fields = ('usuario', 'contrasena', 'remember_me')



class UserUpdateForm(DocumentForm):
    """!
    Clase del formulario de actualización de usuario

    @date 01-05-2018
    @version 1.0.0
    """

    ## Nombre de Usuario
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'validate'}))

    ## Correo
    email = forms.EmailField()

    ## Nombre
    first_name = forms.CharField(required=False)

    ## Apellido
    last_name = forms.CharField(required=False)

    ## Contraseña
    password = forms.CharField(widget=forms.PasswordInput,required=False)

    ## Repetir Contraseña
    password_repeat = forms.CharField(widget=forms.PasswordInput,required=False)

    class Meta:
        document = User
        fields = ['username','email','first_name','last_name']


class UserRegisterForm(DocumentForm):
    """!
    Formulario de Registro

    @date 02-05-2018
    @version 1.0.0
    """

    password2 = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password',
                  'first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)

        self.fields['username'].required = True
        self.fields['username'].label = 'Nombre de Usuario'
        self.fields['username'].widget = forms.TextInput()

        self.fields['password'].required = True
        self.fields['password'].label = 'Constraseña'
        self.fields['password'].widget = forms.PasswordInput()

        self.fields['password2'].required = True
        self.fields['password2'].label = 'Repita su constraseña'

        self.fields['first_name'].label = 'Nombre'

        self.fields['last_name'].label = 'Apellido'

        self.fields['email'].label = 'Correo'

    def clean_password2(self):
        """!
        Método que valida si las contraseñas coinciden

        @date 02-05-2018
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Retorna el campo con la validacion
        """
        password = self.cleaned_data['password']
        password_repeat = self.cleaned_data['password2']
        if(password_repeat!=password):
            raise forms.ValidationError("La contraseña no coincide")
        return password_repeat

    def clean_email(self):
        """!
        Método que valida si el correo es única

        @date 02-05-2018
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Retorna el campo con la validacion
        """
        email = self.cleaned_data['email']
        if(validate_email(email)):
            raise forms.ValidationError("El correo ingresado ya existe")
        return email

    def clean_username(self):
        """!
        Método que valida si el username es único

        @date 02-05-2018
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Retorna el campo con la validacion
        """
        username = self.cleaned_data['username']
        if(validate_username(username)):
            raise forms.ValidationError("El nombre de usuario ingresado ya existe")
        return username
