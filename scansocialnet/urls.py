"""
Sistema de Rastreo de Redes Sociales
Copyright (@) 2018 GoAppsWeb
"""

## @package scansocialnet.url
#
# Urls de la aplicación
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0.0

from django.conf.urls import url, include
from django.contrib import admin
from rest.routers import router

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('base.urls')),
    url(r'^api/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/facebook/', include('facebook_api.urls')),
    url(r'^api/', include('users.urls', namespace='auth')),
    url(r'^api/', include(router.urls)),
]
